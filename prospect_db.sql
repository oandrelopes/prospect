/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : sys_prospect_db

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-03-17 17:57:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `rg` varchar(255) DEFAULT NULL,
  `dataNascimento` varchar(255) DEFAULT NULL,
  `nomeMae` varchar(255) DEFAULT NULL,
  `nomePai` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `telefone2` varchar(255) DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT 'Brasil',
  `dataCadastro` datetime DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES ('1', 'André Luiz Lopes', '012.345.678-90', '01234567', '08/02/1986', 'Maria Lopes', 'Paulo Lopes', 'lopes.andree@gmail.com', null, null, null, '04142-050', 'SP', 'São Paulo', 'Saúde', 'Rua: Traituba', '214', 'Casa 3', 'Brasil', '2015-03-17 15:50:43', '1');
INSERT INTO `cliente` VALUES ('2', 'Maria da Silva', '012.345.679-00', '01234567', '01/01/1970', 'Marcia Silva', 'Joao Silva', 'maria@gmail.com', null, null, null, '82030-000', 'PR', 'Curitiba', 'São João', 'Rua: Ari José Valle', '123', '', 'Brasil', '2015-03-17 15:52:38', '1');

-- ----------------------------
-- Table structure for empresa
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `sobre` text,
  `endereco` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of empresa
-- ----------------------------
INSERT INTO `empresa` VALUES ('1', 'Teste', 'Teste', '01234567890', '1', 'teste', 'Nilton Sales Rosa, 41', 'teste');

-- ----------------------------
-- Table structure for metodos
-- ----------------------------
DROP TABLE IF EXISTS `metodos`;
CREATE TABLE `metodos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classe` varchar(255) DEFAULT NULL,
  `metodo` varchar(255) DEFAULT NULL,
  `apelido` varchar(255) DEFAULT NULL,
  `privado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of metodos
-- ----------------------------
INSERT INTO `metodos` VALUES ('1', 'metodos', 'index', 'Listar Metodos', '1');
INSERT INTO `metodos` VALUES ('2', 'inicio', 'index', 'Listar Prospect', '1');
INSERT INTO `metodos` VALUES ('3', 'inicio', 'incluir', 'Incluir Prospect', '1');
INSERT INTO `metodos` VALUES ('4', 'metodos', 'alterar_apelido', 'Alterar Apelido', '1');
INSERT INTO `metodos` VALUES ('5', 'metodos', 'alterar_privacidade', 'Alterar Privacidade', '1');
INSERT INTO `metodos` VALUES ('6', 'usuarios', 'index', 'usuarios/index', '1');
INSERT INTO `metodos` VALUES ('7', 'usuarios', 'incluir', 'usuarios/incluir', '1');
INSERT INTO `metodos` VALUES ('8', 'usuarios', 'atualizar', 'usuarios/atualizar', '1');
INSERT INTO `metodos` VALUES ('9', 'usuarios', 'listar_permissoes', 'usuarios/listar_permissoes', '1');
INSERT INTO `metodos` VALUES ('10', 'usuarios', 'salvar_permissoes', 'usuarios/salvar_permissoes', '1');
INSERT INTO `metodos` VALUES ('11', 'usuarios', 'update_password', 'usuarios/update_password', '1');
INSERT INTO `metodos` VALUES ('12', 'clientes', 'index', 'clientes/index', '1');
INSERT INTO `metodos` VALUES ('13', 'clientes', 'atualizar', 'clientes/atualizar', '1');
INSERT INTO `metodos` VALUES ('14', 'clientes', 'incluir', 'clientes/incluir', '1');
INSERT INTO `metodos` VALUES ('15', 'clientes', 'verefica_cpf', 'clientes/verefica_cpf', '1');
INSERT INTO `metodos` VALUES ('16', 'clientes', 'filtrar', 'clientes/filtrar', '1');
INSERT INTO `metodos` VALUES ('17', 'usuarios', 'drop_user', 'usuarios/drop_user', '0');
INSERT INTO `metodos` VALUES ('18', 'inicio', 'atualizar', 'inicio/atualizar', '0');

-- ----------------------------
-- Table structure for permissoes
-- ----------------------------
DROP TABLE IF EXISTS `permissoes`;
CREATE TABLE `permissoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_metodo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissoes
-- ----------------------------
INSERT INTO `permissoes` VALUES ('1', '10', '10');
INSERT INTO `permissoes` VALUES ('2', '9', '10');

-- ----------------------------
-- Table structure for prospect
-- ----------------------------
DROP TABLE IF EXISTS `prospect`;
CREATE TABLE `prospect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `setor` varchar(255) DEFAULT NULL,
  `contato` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prospect
-- ----------------------------
INSERT INTO `prospect` VALUES ('1', 'Prospect', '123', '123', null, '2015-03-17 19:49:43', '1');
INSERT INTO `prospect` VALUES ('2', 'AAAAAAAAAA', 'AAAAAAAAAAA', 'AAAAAAAAAA', '2', '2015-03-17 19:51:57', '1');
INSERT INTO `prospect` VALUES ('3', '2131231', '23123123', '123123123', '0', '2015-03-17 20:44:44', '1');

-- ----------------------------
-- Table structure for prospect_copy
-- ----------------------------
DROP TABLE IF EXISTS `prospect_copy`;
CREATE TABLE `prospect_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `setor` varchar(255) DEFAULT NULL,
  `contato` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prospect_copy
-- ----------------------------
INSERT INTO `prospect_copy` VALUES ('1', 'Prospect', '123', '123', null, '2015-03-17 19:49:43', '1');
INSERT INTO `prospect_copy` VALUES ('2', 'AAAAAAAAAA', 'AAAAAAAAAAA', 'AAAAAAAAAA', null, '2015-03-17 19:51:57', '1');
INSERT INTO `prospect_copy` VALUES ('3', '2131231', '23123123', '123123123', null, '2015-03-17 20:44:44', '1');

-- ----------------------------
-- Table structure for prospect_historico
-- ----------------------------
DROP TABLE IF EXISTS `prospect_historico`;
CREATE TABLE `prospect_historico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_contato` datetime DEFAULT NULL,
  `data_reuniao` date DEFAULT NULL,
  `data_final` date DEFAULT NULL,
  `id_prospect` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prospect_historico
-- ----------------------------
INSERT INTO `prospect_historico` VALUES ('1', '2015-03-17 19:49:43', '2015-03-19', '2015-03-31', '1');
INSERT INTO `prospect_historico` VALUES ('2', '2015-03-17 19:51:57', '2015-03-17', '2015-03-17', '2');
INSERT INTO `prospect_historico` VALUES ('3', null, '2015-03-17', '2015-03-17', '2');
INSERT INTO `prospect_historico` VALUES ('4', '2015-03-17 20:44:44', '0000-00-00', '0000-00-00', '3');
INSERT INTO `prospect_historico` VALUES ('5', null, '2015-03-17', '2015-03-19', '3');

-- ----------------------------
-- Table structure for session
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(50) DEFAULT NULL,
  `last_activity` int(10) DEFAULT NULL,
  `user_data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of session
-- ----------------------------
INSERT INTO `session` VALUES ('9b22c41c3e0354fed401fff50ee1c1ba', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', '1426625066', 'a:6:{s:9:\"user_data\";s:0:\"\";s:2:\"id\";s:1:\"1\";s:4:\"name\";s:6:\"André\";s:5:\"email\";s:5:\"admin\";s:10:\"id_empresa\";s:1:\"1\";s:10:\"isLoggedIn\";b:1;}');

-- ----------------------------
-- Table structure for telefone
-- ----------------------------
DROP TABLE IF EXISTS `telefone`;
CREATE TABLE `telefone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2051 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of telefone
-- ----------------------------
INSERT INTO `telefone` VALUES ('2048', '1', '(11) 9486-27556');
INSERT INTO `telefone` VALUES ('2049', '1', '(42) 3226-3100');
INSERT INTO `telefone` VALUES ('2050', '2', '(11) 9111-11112');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT NULL,
  `id_permissao` int(11) DEFAULT '3',
  `id_empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'André', 'admin', 'MWpgtGiEZwoV8jRn5GHGFClQMzo04vt0PRmkiRJUwP6xyRxAHhBGp0yI2SYZvHf0NzV9j9UxNy9NMcE9c6fNnQ==', '1', '1', '1');
INSERT INTO `usuario` VALUES ('10', 'José dos Santos', 'jose@gmail.com', 'CVxGkQWUI1YYQGdA7QwOm9l6Mo8qqk+3GJ/v2nS3oX5v1vgHd+W7OJsJX8Ri1SgwkMBBEmARnNEKBBmvqR765A==', '0', '0', '1');
DROP TRIGGER IF EXISTS `copy_prospect`;
DELIMITER ;;
CREATE TRIGGER `copy_prospect` AFTER INSERT ON `prospect` FOR EACH ROW BEGIN
   INSERT INTO prospect_copy select *  from prospect ORDER BY id DESC LIMIT 1 ;   
 END
;;
DELIMITER ;
