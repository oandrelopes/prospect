$(document).ready(function() {
	
	function converte_data(date) {
		
		var yyyy = date.getFullYear();
	    var mm = date.getMonth() + 1;
	    var dd = date.getDate();
	    var hh = date.getHours();
	    var min = date.getMinutes();
	    var ss = date.getSeconds();
	 
		var mysqlDateTime = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + ss;
		console.log( mysqlDateTime );
		return mysqlDateTime = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + ss;
	}

	// **************************
	// ********* Menu ***********
	// **************************	

	var seletor = $("#menu a")
	$(seletor).click(function(e){
        e.preventDefault();

        var link = $(this).attr("href");
        $('html, body').animate({
            scrollTop: $(link).offset().top
        }, 500);
    });

	// **************************
	// ******** Agenda **********
	// **************************

	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'agendaWeek,month,agendaDay'
		},
		lang: "pt-br",
		defaultView: 'agendaWeek',
		allDaySlot: false,
		minTime: '08:00:00',
		maxTime: '19:00:00',
		slotDuration: '00:30:00',
		slotEventOverlap: false,
		selectable: true,
		selectHelper: false,
		select: function(start, end) {
			
			console.log( start );

			var inicio = converte_data(start._d);
			var fim    = converte_data(end._d);

			$.ajax({
				url: site_url+'agenda/incluir',
				type: 'POST',
				dataType: 'json',
				data: { data_inicial: inicio, data_final: fim, slug_empresa: slug_empresa },
			
			}).fail(function() {
				console.log( 'fail' );
				//$('#calendar').fullCalendar('unselect');

			}).success( function(){
				
				console.log('success');
				 var eventData = {
				 	title: 'Nome Usuário',
				 	start: start,
				 	end: end,
				 	color: '#ccc'
				 };
				$('#calendar').fullCalendar('renderEvent', eventData, true);

			});


			
		},
		editable: false,
		events: {
			url: site_url+'agenda/minha_agenda/'+slug_empresa,
			error: function() {
				$('#script-warning').show();
			}
		},
		loading: function(bool) {
			$('#loading').toggle(bool);
		}
		
	});

	$('.ui.checkbox').checkbox();

	// **************************
	// ********* MAPA ***********
	// **************************

	// var url = GMaps.staticMapURL({
	// 	lat: -23.627028,
	// 	lng: -46.637258,
	// 	zoom: 15,
	// 	markers: [
	// 	  {lat: -23.627028, lng: -46.637258}
	// 	]
	// });

	// $('<img/>').attr('src', url).appendTo('#map');


	// **************************
	// *** Galeria de imagens ***
	// **************************

	// redefine Cycle's updateActivePagerLink function 
	$.fn.cycle.updateActivePagerLink = function(pager, currSlideIndex) { 
	    $(pager).find('li').removeClass('activeLI') 
	        .filter('li:eq('+currSlideIndex+')').addClass('activeLI'); 
	}; 
	         
	$('#slideshow').cycle({ 
	    timeout: 3000, 
	    pager:  '#nav', 
	    pagerAnchorBuilder: function(idx, slide) { 
	        return '<li><a href="#"><img src="' + slide.src + '" width="185" height="135" /></a></li>'; 
	    } 
	});

	// **************************
	// ********* Contato ********
	// **************************

	$('.ui.form')
	  .form({
	    name: {
	      identifier  : 'name',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor, Preencha o campo Nome'
	        }
	      ]
	    },

	    email: {
	      identifier  : 'email',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor, Preencha o campo E-mail'
	        }
	      ]
	    },

	    tel: {
	      identifier  : 'tel',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor, Preencha o campo Telefone'
	        }
	      ]
	    },

	    msg: {
	      identifier  : 'msg',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor, Preencha o campo Mensagem'
	        }
	      ]
	    }


	  })
	;

	
});
