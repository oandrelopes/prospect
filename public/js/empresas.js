$(function() {
	"use strict";

	$('#ed_nome').blur(function(event) {
		/* Act on the event */
		var self = $(this);
		var slug_atual = $('#ed_slug').val();
		var nome_empresa = self.val();

		if ( nome_empresa.length >= 3 && slug_atual.length < 3 ) {
			self.parents('.field').removeClass('error');
			$.ajax({
				url: site_url+'empresa/create_unique_slug/',
				type: 'POST',
				dataType: 'json',
				data: {nome_empresa: nome_empresa},
			})
			.success(function( data ) {
				$('#ed_slug').val(data).attr('readonly','readonly').parents('.field').removeClass('error').slideDown('fast');
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});

		} else {
			//self.parents('.field').addClass('error');
		}
		
	
	});

	$('.editar-slug').click(function(event) {
		/* Act on the event */
		$('#ed_slug').removeAttr('readonly');
	
	});

	$('#ed_slug').blur(function(event) {
		/* Act on the event */
		var self = $(this);
		var nome_empresa = self.val();

		if ( nome_empresa.length >= 3 ) {
			$.ajax({
				url: site_url+'empresa/create_unique_slug/',
				type: 'POST',
				dataType: 'json',
				data: {nome_empresa: nome_empresa},
			})
			.success(function( data ) {
				$('#ed_slug').val(data).attr('readonly','readonly').removeClass('error');
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});

		} else {
			$('#ed_slug').val('');
			self.parents('.field').addClass('error').attr('readonly','readonly');
		}

	});

});