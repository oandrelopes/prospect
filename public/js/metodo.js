jQuery(document).ready(function($) {    

    $('.button.salvar, .button.cancelar').click(function(event) {
        /* Act on the event */        
        $('.ui.form').addClass('loading');
    });

    $(".checkbox_privacidade").click( function() {
        var self = $(this);
        var url = url_atual+"/alterar_privacidade";
        var id_metodo = self.attr('data-idmetodo');
        var privado = self.find("input[name='check_privado']:checked").val();

        $.ajax({
                url: url,
                type: 'POST',
                //dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                data: {id_metodo: id_metodo, privado: privado},
            })
            .done(function() {
                $('.mensagem-ajax').slideDown('fast');
            })
            .fail(function() {
                $('.mensagem-ajax').slideDown('fast');
            })
            .always(function() {
                
            });

    });
    
    $('.edit_apelido').click(function(event) {
        /* Act on the event */
        var self = $(this);
        self.next('.icon-salvar-apelido').css('display', 'inline-block');
    });
    $('.icon-salvar-apelido').click(function(event) {
        /* Act on the event */
        var self = $(this);
        var edit_apelido = self.prev('.edit_apelido');
        var id_metodo = edit_apelido.attr('data-idmetodo');
        var apelido = $.trim(edit_apelido.text());
        var url;
        
        if ( apelido.length >= 3 ) { 

            url = url_atual+"/alterar_apelido";

            $.ajax({
                        url: url,
                        type: 'POST',
                        data: {id_metodo: id_metodo, apelido: apelido},
                    })
                    .done(function() {
                        $('.mensagem-ajax').slideDown('fast');
                        self.fadeOut();
                    })
                    .fail(function() {
                        $('.mensagem-ajax').slideDown('fast');
                    })
                    .always(function() {
                        
                    });

        } 

    });

    $('.edit_apelido').on('keyup', function(e) {
        if ( event.which == 13 || event.keyCode == 13 ) {
            e.preventDefault();
            //$('.edit_apelido').trigger('blur');
        }
    });

    $( document ).ajaxStart(function() {
        $('.ui.form').addClass('loading');
    });

    $(document).ajaxStop( function() {        
        $('.ui.form').removeClass('loading');
    });
        

});


