$(document).ready(function(){
    // Altera os compos para cadastro de pessoa fisica e juridica
    $('input[name=ed_tipo]').click(fisicaJuridica);

    // Filtra as cidades qualdo se clica em option do estado
    $('#ed_estado').change(filtrarCidade);

    // Padr�o das mascaras
    $("#ed_cpf").mask("999.999.999-99");
    $("#ed_cnpj").mask("99.999.999/9999-99");
    $("#ed_telefone").mask("(99) 9999-9999");
    $("#ed_celular").mask("(99) 9999-9999");
    $("#ed_cep").mask("99999-999");

});

// Fun��o que exibe e esconde os campos referentes a pessoa juridica e f�sica
function fisicaJuridica(){
    tipo = $('input[name=ed_tipo]:checked').val();
    if(tipo == 'f'){
        // A��es a serem tomadas caso a pessoa seja f�sica
        $('.cpf').css('display','block');
        
        $('.cnpj').css('display','none');
        $('.razao_social').css('display','none');
    }
    else{
        // A��es a serem tomadas caso a pessoa seja jur�dica
        $('.cnpj').css('display','block');
        $('.razao_social').css('display','block');

        $('.cpf').css('display','none');
        
    }
}

fisicaJuridica();

function filtrarCidade(){
    estado = $('#ed_estado option:selected').val();

    $('#ed_cidade').html('<option value="">Carregando...</option>');
    $.post(url + '/clients/city_list/', {'ed_estado' : estado}, function(data){
        saida = '';
        for(i = 0 ; i < data.length ; i++){
            if(estado == 'PR' && data[i].nome == 'Ponta Grossa')
                saida += '<option selected="selected" value="' + data[i].cod_cidade + '">' + data[i].nome + '</option>';
            else
                saida += '<option value="' + data[i].cod_cidade + '">' + data[i].nome + '</option>';

        }

        $('#ed_cidade').html(saida);
    }, 'json');
}

filtrarCidade();

