// Configura��o para o calend�rio
$('#ed_data_envio').datepicker({
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    monthNames: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    dateFormat: 'dd/mm/yy'
});

$('#ed_data_retorno').datepicker({
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    monthNames: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    dateFormat: 'dd/mm/yy'
});

$('#ed_data_retorno_cliente').datepicker({
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    monthNames: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    dateFormat: 'dd/mm/yy'
});

$(document).ready(function(){
    // Altera os compos para cadastro de pessoa fisica e juridica
    $('input[name=ed_tipo]').click(fisicaJuridica);

    // Filtra as cidades quando se clica em option do estado
    //$('#ed_estado').change(filtrarCidade);

    // Padr�o das mascaras
    $("#ed_cpf").mask("999.999.999-99");
    $("#ed_cnpj").mask("99.999.999/9999-99");
    $("#ed_telefone").mask("(99) 9999-9999");
    $("#ed_celular").mask("(99) 9999-9999");
    $("#ed_cep").mask("99999-999");

});

// Abilita os campos para edicao
function updateClient(){

    $('.form-horizontal input[type=text]').removeAttr('disabled');
    $('.form-horizontal select').removeAttr('disabled');
    $('.form-horizontal input[type=radio]').removeAttr('disabled');
    
    $('#atualizar_cliente').css('display', 'none');
    $('#salvar_cliente').css('display', 'inline');
}

$('#atualizar_cliente').click(function(){
    updateClient();
});

// Fun��o que exibe e esconde os campos referentes a pessoa juridica e f�sica
function fisicaJuridica(){
    tipo = $('input[name=ed_tipo]:checked').val();
    if(tipo == 'f'){
        // A��es a serem tomadas caso a pessoa seja f�sica
    	$('.cpf').css('display','block');
        
        $('.cnpj').css('display','none');
        $('.razao_social').css('display','none');
    }
    else{
        // A��es a serem tomadas caso a pessoa seja jur�dica
        $('.cnpj').css('display','block');
        $('.razao_social').css('display','block');

        $('.cpf').css('display','none');
    }
}

fisicaJuridica();

// Fun��o filtra as cidades por ajax
/*function filtrarCidade(){
    // Pega o estado selecionado
    estado = $('#ed_estado option:selected').val();

    // Informa ao usu�rio que est� carregando as cidades
        if(add == false)
        $('#ed_cidade').html('<option value="">Carregando...</option>');

    // envia o POST para o servidor
    $.post('http://localhost/callcenter_final/clients/city_list/', {'ed_estado' : estado}, function(data){
        saida = '';
        for(i = 0 ; i < data.length ; i++){
            saida += '<option value="' + data[i].cod_cidade + '">' + data[i].nome + '</option>';
        }

        // Imprime no select as cidades filtradas
        if(add == true){
            $('#ed_cidade').append(saida);
            add = false;
        }
        else
            $('#ed_cidade').html(saida);
    }, 'json');
}
add = true;
filtrarCidade();

$('#ed_assunto').change(exibirCampo);
*/
function exibirCampo(){
    cod_assunto = $('#ed_assunto').val();

    // Assuntos que possuem guias
    guia = new Array(
        1, 16, 17, 18, 20, 21, 22, 23, 24, 32, 35, 36, 37, 40, 41, 42, 43, 44
    );

    var t = 0;
    for(i = 0 ; i < guia.length ; i++){
        if(guia[i] == cod_assunto)
            t++;
    }

    // Verifica se o assunto � igual a Assist�ncia, se sim exibe o campo fornecedor e guia
      if(t > 0){
    	$('.fornecedor').css('display', 'block');
    	$('.guia').css('display', 'block');
    }
    else{
    	$('.fornecedor').css('display', 'none');
    	$('.guia').css('display', 'none');
    	$('#ed_guia').val('');
    }
}

exibirCampo();



var last_cep = 0;
var address;
var lat;
var lng;
var wsconf;
function wscep(conf)
{

    //parametros padrao true
    if(!conf){
        conf = {
            'auto': true,
            'map' : '',
            'wsmap' : ''
        };
    }
    wsconf = conf;
    //evento keyup no campo cep opcional
    if(wsconf.auto == true){
        $('#ed_cep').live('keyup',function(){
            var cep = $.trim($('#ed_cep').val()).replace('_','');
            if(cep.length >= 9){
                if(cep != last_cep){
                    busca();
                }
            }
        });         
    }else{
        var btn_busca = '<div class="input-append input-prepend"><span class="add-on">CEP</span>';
        btn_busca += '<input id="cep" name="cep" style="width:139px!important" type="text" maxlength="9" placeholder="Informe o CEP" />';
        btn_busca += '<button class="btn btn_handler" type="button">Busca</button></div>';
        $('#cep-label').replaceWith(btn_busca)
        $('.btn_handler').live('click',function(){
            busca();
        })
    }    
    
}
//busca o cep
function busca(){
    var cep = $.trim($('#ed_cep').val());    
    var url = 'http://xtends.com.br/webservices/cep/json/'+cep+'/';    
   /* if ($.browser.msie) {
        var url = 'ie.php';    
    }*/
    $.post(url,{cep:cep},
        function (rs) {
            rs = $.parseJSON(rs);
            if(rs.result == 1){
                address = rs.logradouro + ', ' + rs.bairro + ', ' + rs.cidade + ', ' + ', ' + rs.uf;
                /*if(wsconf.map != '' ){
                    setMap(wsconf.map);
                }*/
                $('#ed_endereco').val(rs.logradouro);
                $('#ed_bairro').val(rs.bairro);
                $('#ed_cidade').val(rs.cidade);
                $('#ed_uf').val(rs.uf);
                $('#ed_cep').removeClass('invalid');
                $('#ed_numero').focus();
                $('#ed_numero').live('change',function(){
                    address = rs.logradouro + ', ' + $('#ed_numero').val() + ', ' + rs.bairro + ', ' + rs.cidade + ', ' + ', ' + rs.uf;    
                    /*if(wsconf.map != ''){
                        setMap(wsconf.map);
                    }*/
                })
                last_cep = cep;
            }
            else{
                $('#ed_cep').addClass('invalid');    
                $('#ed_cep').focus();  
                last_cep = 0;
            }
        })    
}
 
