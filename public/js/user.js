jQuery(document).ready(function($) {    

    $('.button.salvar, .button.cancelar').click(function(event) {
        /* Act on the event */        
        $('.ui.form').addClass('loading');
    });

    $('.ui.form').form({
        nome: {
            identifier: 'ed_nome',
            rules: [{
                type: 'empty',
                prompt: 'Please enter your first name'
            }, {
                type: 'length[10]',
                prompt: 'Your password must be at least 6 characters'
            }]
        },
        email: {
            identifier: 'ed_email',
            rules: [{
                type: 'empty',
                prompt: 'Please enter your first name'
            },{
                type: 'email',
                prompt: 'Email'
            }]
        },
        senha: {
            identifier: 'ed_senha',
            rules: [{
                type: 'empty',
                prompt: 'Please enter a password'
            }, {
                type: 'length[6]',
                prompt: 'Your password must be at least 6 characters'
            }]
        }, 
        permissao: {
        	identifier: 'ed_tipo_usuario',
        	rules: [{
        		type: 'empty',
        		prompt: 'Selecione a permissao'
        	}]
        }

    }, {
        inline : true,
        //on     : 'blur',
        onInvalid: function() { $('.ui.form').removeClass('loading') }
    });

    $(".checkbox-permissao").click( function() {
        
        var url = site_url+"usuarios/salvar_permissoes";
        var id_user   = $(this).attr('data-iduser');
        var id_metodo = $(this).attr('data-idmetodo');
        var ativo = 'false';
        if ( $(this).find("input[name='ed_permissao']").is(':checked') ) {
            ativo = 'true';
        }

        $.ajax({
                    url: url,
                    type: 'POST',
                    //dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {id_user: id_user, id_metodo: id_metodo, ativo: ativo},
                })
                .done(function() {
                    $('.ui.green.message').transition('scale', '1000ms').transition('scale', '1000ms');
                })
                .fail(function() {
                    $('.ui.red.message').transition('scale', '1000ms').transition('scale', '1000ms');
                })
                .always(function() {
                    
                });
                        
    });

    $( document ).ajaxStart(function() {
        $('.ui.form').addClass('loading');
    });

    $(document).ajaxStop( function() {        
        $('.ui.form').removeClass('loading');
    });
        

});


