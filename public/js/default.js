/****
    André Luiz Lopes - 11 948627556
    lopes.andree@gmail.com
****/
window.MAIN = window.MAIN || {};
MAIN.defaultJs = MAIN.defaultJs || {};

/*VARIAIS GLOBAIS*/
var executedClass = false;
var url_atual     = window.location.href; 


(function($){
    
    "use strict";

    MAIN.defaultJs.ClassJs = function(){

        /*variaveis*/
        var windowTarget, link;
        var dropdown,
            formulario,
            checkbox,
            msg_ajax,
            popup_btn,
            calendario;


        function goTo(target){
            windowTarget.animate({scrollTop: $(target).offset().top}, 500, function(){
                //window.location.hash = target;
            });
        } 

        
        return {
            
            init: function() {

                windowTarget = $("html:not(:animated),body:not(:animated)"); //janela atual     
                link         = $('class or id');
                dropdown     = $(".ui.dropdown");
                formulario   = $('.ui.form');
                checkbox     = $('.ui.checkbox');
                msg_ajax     = $('.mensagem-ajax');
                popup_btn    = $('.popup_btn');
                calendario   = $('.calendario');

                this.bind();
                //this.teste();


            },

            bind: function() {
                
                dropdown.dropdown();
                formulario.removeClass('loading');
                checkbox.checkbox('toogle');
                popup_btn.popup({
                        /*position : 'top right',
                        target   : '.test.image',
                        title    : 'My favorite dog',
                        content  : 'My favorite dog would like other dogs as much as themselves'*/
                        variation : 'inverted'
                    });

                link.click( function(e) {
                    e.preventDefault();
                    goTo( $(this).attr('href') );
                });



                calendario.datepicker({
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    dateFormat: 'yy-mm-dd'
                });

                //calendario.mask("99/99/9999");

                $(document,'window').click(function(event) {
                    /* Act on the event */
                    msg_ajax.slideUp();
                });

            }, /*bind*/

            teste: function() {
                console.log( 'ENTROU TESTE' );
            } /*teste*/
        };
    }; /*classe principal*/
    
    $(window).load(function(){
        if(!executedClass)
        {

            MAIN.defaultJs.StaticModule = new MAIN.defaultJs.ClassJs();
            MAIN.defaultJs.StaticModule.init();
            window.executedClass = true;
            
        }
    });
    
}( jQuery ) );

