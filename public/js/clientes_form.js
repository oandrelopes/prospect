$(document).ready(function(){    

    var telefone = $('.telefone');

     $('#ed_dataNascimento').datepicker({
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        dateFormat: 'dd/mm/yy'
    });
    // Padrão das mascaras
    $("#ed_dataNascimento").mask("99/99/9999");
    $("#ed_cpf").mask("999.999.999-99");
    telefone.mask("(99) 9999-9999?9");
    $("#ed_cep").mask("99999-999");

     wscep();

    var i = $('#telefones input').size() + 1;
    $('#ed_total_tel').val(i);

});


// Abilita os campos para edicao
$('.atualizar_cliente').click(function(event) {
    event.preventDefault();
    var self = $(this);
    $('.field input[type=text]').removeAttr('disabled');
    $('.field select').removeAttr('disabled');
    $('.field input[type=radio]').removeAttr('disabled');
    
    $('#add').css('display', '');
    $('#remove').css('display', '');

    self.fadeOut('slow', function() {
        $('.salvar_cliente').fadeIn('slow');
    });
    
});



var last_cep = 0;
var address;
var lat;
var lng;
var wsconf;
function wscep(conf)
{

    //parametros padrao true
    if(!conf){
        conf = {
            'auto': true,
            'map' : '',
            'wsmap' : ''
        };
    }
    wsconf = conf;
    //evento keyup no campo cep opcional
    if(wsconf.auto == true){
        $('#ed_cep').on('keyup',function(){
            var cep = $.trim($('#ed_cep').val()).replace('_','');
            if(cep.length >= 9){
                if(cep != last_cep){
                    busca();
                }
            }
        });         
    }else{
        var btn_busca = '<div class="input-append input-prepend"><span class="add-on">CEP</span>';
        btn_busca += '<input id="cep" name="cep" style="width:139px!important" type="text" maxlength="9" placeholder="Informe o CEP" />';
        btn_busca += '<button class="btn btn_handler" type="button">Busca</button></div>';
        $('#cep-label').replaceWith(btn_busca)
        $('.btn_handler').on('click',function(){
            busca();
        })
    }    
    
}
//busca o cep
function busca(){
    var cep = $.trim($('#ed_cep').val());    
    //var url = 'http://xtends.com.br/webservices/cep/json/'+cep+'/';    
    var url = 'http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep='+cep;    
    
    $.getScript( url, function(){         

        
        if ( resultadoCEP["resultado"] == 1 ) {

            // troca o valor dos elementos
            $("#ed_endereco").val(unescape(resultadoCEP["tipo_logradouro"]) + ": "+unescape(resultadoCEP["logradouro"]));
            $('#ed_bairro').val(unescape(resultadoCEP["bairro"]));
            $('#ed_cidade').val(unescape(resultadoCEP["cidade"]));
            $('#ed_uf').val(unescape(resultadoCEP["uf"]));
            $('#ed_cep').parents('.form-group').removeClass('has-error');
            $('#ed_numero').focus();
            $('#ed_numero').on('change',function(){
                address = rs.logradouro + ', ' + $('#ed_numero').val() + ', ' + rs.bairro + ', ' + rs.cidade + ', ' + ', ' + rs.uf;    
                
            })
            
        } else {
            $('#ed_cep').val('').focus();  
            $('#ed_cep').parents('.form-group').addClass('has-error');    
            last_cep = 0;
            $("#ed_endereco").val('');
            $('#ed_bairro').val('');
            $('#ed_cidade').val('');
            $('#ed_uf').val('');
        }

    });
    

    

   
    $.post(url,{cep:cep},
        function (rs) {
            rs = $.parseJSON(rs);
            if(rs.result == 1){
                address = rs.logradouro + ', ' + rs.bairro + ', ' + rs.cidade + ', ' + ', ' + rs.uf;
                
                $('#ed_endereco').val(rs.logradouro);
                $('#ed_bairro').val(rs.bairro);
                $('#ed_cidade').val(rs.cidade);
                $('#ed_uf').val(rs.uf);
                
                last_cep = cep;
            }
            else{
                
            }
        })    
}

/*verefica se cpf ja existe*/
$('#ed_cpf').blur(function(){
    var url = location.host;
    var cpf = $('#ed_cpf').val();        
    
    $.post( site_url+'clientes/verefica_cpf/'+cpf,function(data){ 

        if ( data === true ) {
            
            $('#myModal').modal();
            $('#ed_nome').focus();
            $('#divCpf').addClass('error');

            $('.modal.modal-cpf')
                .modal('setting', 'closable', false)
                .modal('show')
            ;

        } else {

            $('#divCpf').removeClass('error');

        }

    }, 'json');
    

});


$('#adicionarTelefone').click(function(){
    adicionaTelefone();
});

$('#removerTelefone').click(function(){
    removeTelefone();
});
 
    var i = $('#telefones input').size() + 1; // check how many input exists on the document and add 1 for the add command to work   
    $('a#add').click(function() { // when you click the add link
        if ( $('#telefones *').filter(':animated').length == 0 ) { //verefica se esta adicionando
            $('<div class="field"><input type="text" class="form-control telefone" id="ed_telefone'+i+'" name="ed_telefone'+i+'" placeholder="Telefone ' + (i+1) + '" /></div>').appendTo('#telefones').hide().show(600);//.slideDown();//.show('slow'); // append (add) a new input to the document.
            $('#ed_telefone'+i).mask("(99) 9999-9999?9");
            // if you have the input inside a form, change body to form in the appendTo
            i++; //after the click i will be i = 3 if you click again i will be i = 4
            $('#ed_total_tel').val(i);
        }
    });

    $('a#remove').click(function() { // similar to the previous, when you click remove link
    if(i > 1) { // if you have at least 1 input on the form
        $('#telefones input:last').slideUp(600, function(){
            $('#telefones input:last').remove(); //remove the last input
            $('#telefones .field:last').remove(); 
            i--; //deduct 1 from i so if i = 3, after i--, i will be i = 2    
            $('#ed_total_tel').val(i);
        })
        
    }
    });

    $('a.reset').click(function() { 
    while(i > 2) { // while you have more than 1 input on the page
        $('input:last').remove(); // remove inputs
        i--;
    }
    });  
