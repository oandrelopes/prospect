$(document).ready(function() {

	$('.menu .item').tab({history:false});

    $(".check_status_agenda").click( function() {
        
        var self      = $(this);
        var id_agenda = self.attr('data-id-agenda');
        var status    = self.find("input[name='check_status_agenda']").is(':checked');
        
        $.ajax({
                url: url_atual+'/atualizar_status',
                type: 'POST',
                //dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                data: {id_agenda: id_agenda, status: status},
            })
            .success(function() {
                $('.mensagem-ajax').slideDown('fast');
            })
            .fail(function() {
                $('.mensagem-ajax').slideDown('fast');
            })
            .always(function() {
                
            });

    });

});
