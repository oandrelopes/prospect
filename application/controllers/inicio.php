<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

    function __construct() {
        // constructor
        parent::__construct();
        $this->load->helper( array('url', 'form', 'default') );
        $this->load->model('prospect_model', 'obj');
        $this->load->library( array('permissoes', 'form_validation') );

        $result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );
        $this->obj->idEmpresa = $this->session->userdata('id_empresa');

        if ( !$result || !$this->obj->idEmpresa )
            redirect(base_url().'login', 'refresh');
    }

    function index( $teste = null ) {

        $var = array(
            'title'    => 'Lista de Prospect',
            'prospect' => $this->obj->listar(),
            'js'       => 'progress.js|inicio.js'
        );

        $this->load->view('inicio/listar', $var);

    }

    function incluir() {

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

            $this->obj->nome    = $this->input->post('p_nome');
            $this->obj->setor   = $this->input->post('p_setor');
            $this->obj->contato = $this->input->post('p_contato');
            $this->obj->status  = $this->input->post('p_status');
            $this->obj->data_reuniao  = $this->input->post('p_data_reuniao');
            $this->obj->data_final    = $this->input->post('p_data_final');

            if ( $this->validate_prospect() ) {
                if ( $this->obj->incluir() )
                    redirect(site_url());
            }
        }
    
        $var = array(
            'css'   => 'jquery-ui.min.css',
            'js'    => 'jquery.maskedinput.min.js|jquery-ui.min.js',
            'title' => 'Novo Prospect'
        );

        $this->load->view('inicio/form', $var);
    }

    function atualizar($id) {

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

            $this->obj->id      = $id;
            $this->obj->nome    = $this->input->post('p_nome');
            $this->obj->setor   = $this->input->post('p_setor');
            $this->obj->contato = $this->input->post('p_contato');
            $this->obj->status  = $this->input->post('p_status');
            $this->obj->data_reuniao  = $this->input->post('p_data_reuniao');
            $this->obj->data_final    = $this->input->post('p_data_final');

            if ( $this->validate_prospect() ) {
                if ( $this->obj->atualizar() )
                    redirect(site_url());
            }
        }
    
        $var = array(
            'css'   => 'jquery-ui.min.css',
            'js'    => 'jquery.maskedinput.min.js|jquery-ui.min.js',
            'title' => 'Atualizar Prospect',
            'prospect' => $this->obj->buscar($id)
        );

        $this->load->view('inicio/form', $var);
    }

    function validate_prospect() {

        $config = array(
            array(
                'field' => 'p_nome',
                'label' => 'Nome',
                'rules' => 'required|min_length[3]|max_length[100]'
            ),
            array(
                'field' => 'p_setor',
                'label' => 'Setor',
                'rules' => 'required|min_length[3]|max_length[100]'
            )
        );

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<div class="ui red pointing above ui label">', '</div>');

        return $this->form_validation->run() == true;
    }
}