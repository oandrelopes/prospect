<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct() {
    	parent::__construct();
        $this->load->helper( array('url', 'form', 'default','text') );
        $this->load->library( array('form_validation', 'permissoes', 'upload') );

        /*SEGURANCA*/
        $this->idEmpresa = $this->session->userdata('id_empresa');
		$result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );

        if ( !$result || !$this->idEmpresa ) {
            redirect(base_url().'login', 'refresh');
        }
        /*SEGURANCA*/
   }

  	//if index is loaded
	public function index() {
		//Set the message for the first time
		$data = array('msg' => "Upload File");
    
    	$data['upload_data'] = '';
    
		//load the view/upload.php with $data
		$this->load->view('upload', $data);
    
		
	}


}