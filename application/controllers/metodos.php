<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Metodos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        // this is your constructor  
        parent::__construct();
        $this->load->helper( array('url', 'form', 'default') );
        $this->load->model('metodos_model'   , 'obj');

        $this->load->library( array('permissoes') );
        $result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );
        $this->obj->idEmpresa = $this->session->userdata('id_empresa');

        if ( !$result || !$this->obj->idEmpresa )
            redirect(base_url().'login', 'refresh');
    }

	public function index()
	{
		$var = array(
            'js'      => 'metodo.js',
            'metodos' => $this->obj->listar_metodos()
        );

		$this->load->view('metodos/list', $var);
	}

	function alterar_privacidade() {
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {            
            $id_metodo = $this->input->post('id_metodo');
            $privado = $this->input->post('privado');            
            $this->obj->alterar_privacidade( $id_metodo, $privado );
        }
	}

	function alterar_apelido() {
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {            
            $id_metodo = $this->input->post('id_metodo');
            $apelido = $this->input->post('apelido');            
            $this->obj->alterar_apelido( $id_metodo, $apelido );
        }
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */