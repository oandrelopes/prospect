<?php

class Agenda extends CI_Controller {

    function __construct() {
        // this is your constructor  
        parent::__construct();
        $this->load->helper( array('url', 'form', 'default') );
        $this->load->model('agenda_model', 'obj');
        $this->load->library( array('form_validation', 'pagination', 'permissoes') );
        $this->obj->idEmpresa = $this->session->userdata('id_empresa');
        $result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );

        if ( !$result || !$this->obj->idEmpresa )
            redirect(base_url().'login', 'refresh');

    }



    public function index($posicao = 0) {
        $quantidade = 10;
    	
        $var = array(
            'js'     => 'agenda.js',
            'agenda' => $this->obj->listar(),
            //'empresas_total'   => $this->obj->empresas_total
        );
        
        $config = array(
            'base_url'        => site_url('clientes/index') . '/',
            //'total_rows'      => $this->obj->clientCount,
            'per_page'        => $quantidade,
            'next_link'       => 'próxima',
            'prev_link'       => 'anterior',
            'last_link'       => 'última',
            'first_link'      => 'primeira',
            'first_tag_open'  => '<a class="item" ><i>',
            'first_tag_close' => '</i></a>',
            'last_tag_open'   => '<a class="item"><i>',
            'last_tag_close'  => '</i></a>',
            'next_tag_open'   => '<div>',
            'next_tag_close'  => '</div>',
            'cur_tag_open'    => '<a class="active item">',
            'cur_tag_close'   => '</a>',
            'num_tag_open'    => '<a class="item"><i>',
            'num_tag_close'   => '</i></a>'
        );

        $this->pagination->initialize($config); 
    
        $this->load->view('agenda/list', $var);
    }

    public function atualizar_status() {

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $this->obj->id_agenda = $this->input->post('id_agenda');
            $status               = $this->input->post('status');
            $status = ( $status && $status == 'true' ? 0 : 1 );
            $this->obj->status    = $status;
            $this->obj->atualizar_status();
        }

    }

    public function incluir() {

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            
            $id_empresa = $this->permissoes->get_empresa($this->input->post('slug_empresa'));

            $this->obj->id_empresa    = $id_empresa->id;
            $this->obj->id_cliente    = 1;//fazer
            $this->obj->id_categoria  = 1;//fazer
            $this->obj->data_inicial  = $this->input->post('data_inicial');
            $this->obj->data_final    = $this->input->post('data_final');
            $this->obj->data_cadastro = date('Y-m-d H:i:s');
            $this->obj->status        = 0;
            //$this->obj->slug          = $this->input->post('slug_empresa');
            
            echo json_encode( $this->obj->incluir() );
        
        }

        die();

    }

    public function minha_agenda( $slug_empresa = false ) {
        if ( $slug_empresa )
            echo json_encode( $this->obj->listar_principal($slug_empresa) );
        else
            echo json_encode('error');
        die();
    }

    public function atualizar($id_empresa, $pg = 0){
        $quantidade = 10;

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $this->obj->nome   = $this->input->post('ed_nome');
            $this->obj->cnpj   = $this->input->post('ed_cnpj');
            $this->obj->slug   = $this->input->post('ed_slug');            
            $this->obj->status = ($this->input->post('ed_status') == 'on')?'1':'0';
            $this->obj->id_empresa = $id_empresa;

            $qtdTelefone = $this->input->post('ed_total_tel');
            $i = 0;
            while ($qtdTelefone > $i) {
                if($this->input->post('ed_telefone'.$i)){
                    $telefones[] = $this->input->post('ed_telefone'.$i);
                    
                }$i++;
            }

            if ( $this->validate_empresa() ) {
                $this->obj->atualizar_empresa();
                //$this->obj->updateTelefone($idCliente,$telefones);
            }
        }

        $var = array(
            'js'      => '',
            'css'     => '',
            'empresa' => $this->obj->busca_empresa($id_empresa),
            'title'   => 'Atualizar Empresa'
        );

        $config = array(
            'base_url'    => site_url('empresas/atualizar/' . $id_empresa) . '/',
            'per_page'    => $quantidade,
            'next_link'   => 'próxima',
            'prev_link'   => 'anterior',
            'last_link'   => 'última',
            'first_link'  => 'primeira',
            'uri_segment' => 4 ,
            'full_tag_open'  => '<a class="icon item">',
            'full_tag_close' => '</a>'
        );

        $this->pagination->initialize($config);
        $this->load->view('empresas/form', $var);

    }

    public function filtro($posicao = 0){
        $quantidade = 10;
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $this->obj->filter = $this->input->post('ed_filtro');
        }

        $var = array(
            'js'      => 'clients_list.js',
            'clients' => $this->obj->clientList($posicao, $quantidade),
            'count'   => $this->obj->clientCount,            
            'busca'   => $this->obj->filter
        );
        
         $config = array(
            'base_url'   => site_url('clients/index') . '/',
            'total_rows' => $this->obj->clientCount,
            'per_page'   => $quantidade,
            'next_link'  => 'próxima',
            'prev_link'  => 'anterior',
            'last_link'  => 'última',
            'first_link' => 'primeira'
        );
        
        $this->pagination->initialize(); 

        $this->load->view('clientes/list', $var);
    }

    public function verefica_cpf(){     
        echo json_encode($this->obj->vereficaCpf());
    }

    // Apaga o cliente
    public function drop_client($cod_cliente){
        $this->obj->cod_cliente = $cod_cliente;

        if($this->obj->dropClient())
            redirect(site_url('clients'));
    }
    

    public function validate_empresa(){

            $config = array(
              
                array(
                    'field' => 'ed_nome',
                    'label' => 'Nome',
                    'rules' => 'trim|required|min_length[5]|max_length[100]'
                ),
                array(
                    'field' => 'ed_cnpj',
                    'label' => 'CNPJ'
                )
            );

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters("<p><span class='label label-danger'>", "</span></p>");

        return $this->form_validation->run();
    }
}
