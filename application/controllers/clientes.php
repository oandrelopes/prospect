<?php

class Clientes extends CI_Controller {

    function __construct() {
        // this is your constructor  
        parent::__construct();
        $this->load->helper( array('url', 'form', 'default') );
        $this->load->model('clientes_model', 'obj');
        $this->load->library( array('pagination','permissoes','form_validation') );
        $result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );
        $this->obj->idEmpresa = $this->session->userdata('id_empresa');
    }    

    // Apaga o cliente
    public function drop_client($cod_cliente){
        $this->obj->cod_cliente = $cod_cliente;

        if($this->obj->dropClient())
            redirect(site_url('clients'));
    }

    public function index($posicao = 0) {
        $quantidade = 10;
    	
        $var = array(
            'js'      => '',
            'clients' => $this->obj->listar($posicao, $quantidade),
            'count'   => $this->obj->clientCount
        );
        
        $config = array(
            'base_url'        => site_url('clientes/index') . '/',
            'total_rows'      => $this->obj->clientCount,
            'per_page'        => $quantidade,
            'full_tag_open'   => '<div class="ui pagination menu">',
            'full_tag_close'  => '</div>',
            'first_link'      => FALSE,
            'last_link'       => FALSE,
            'first_tag_open'  => '<div class="item">',
            'first_tag_close' => '</div>',
            'prev_link'       => '<i class="icon left arrow"></i>',
            'prev_tag_open'   => '<div class="icon item">',
            'prev_tag_close'  => '</div>',
            'next_link'       => '<i class="icon right arrow"></i>',
            'next_tag_open'   => '<div class="icon item">',
            'next_tag_close'  => '</div>',
            'last_tag_open'   => '<div class="item">',
            'last_tag_close'  => '</div>',
            'cur_tag_open'    => '<a class="active item">',
            'cur_tag_close'   => '</a>',
            'num_tag_open'    => '<div class="item">',
            'num_tag_close'   => '</div>'
        );

        $this->pagination->initialize($config); 
    
        $this->load->view('clientes/list', $var);
    }

    public function incluir() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { 

            $this->obj->nome            = $this->input->post('ed_nome');
            $this->obj->cpf             = $this->input->post('ed_cpf');
            $this->obj->rg              = $this->input->post('ed_rg');
            $this->obj->dataNascimento  = $this->input->post('ed_dataNascimento');
            $this->obj->nomeMae         = $this->input->post('ed_nomeMae');
            $this->obj->nomePai         = $this->input->post('ed_nomePai');
            $this->obj->email           = $this->input->post('ed_email');
            $this->obj->telefone        = $this->input->post('ed_telefone');
            $this->obj->telefone2       = $this->input->post('ed_celular');
            $this->obj->cep             = $this->input->post('ed_cep');
            $this->obj->estado          = $this->input->post('ed_uf');
            $this->obj->cidade          = $this->input->post('ed_cidade');
            $this->obj->bairro          = $this->input->post('ed_bairro');
            $this->obj->logradouro      = $this->input->post('ed_endereco');
            $this->obj->numero          = $this->input->post('ed_numero');
            $this->obj->complemento     = $this->input->post('ed_complemento');
            $this->obj->tipo            = ($this->input->post('ed_tipo') == 'j')?'t':'f';
            $this->obj->data_cadastro   = date('Y-m-d H:i:s');



            $qtdTelefone = $this->input->post('ed_total_tel');
            $i = 0;               
            while ($qtdTelefone > $i) {
                
                if ( $this->input->post('ed_telefone'.$i) ) {
                    $telefones[] = $this->input->post('ed_telefone'.$i);                    
                } 
                $i++;
            }  
           
          
            /*metodo para pessoa fisica ou juridica, implementar depois
            $this->obj->tipo        = ($this->input->post('ed_tipo') == 'j')?'t':'f';

            if($this->input->post('ed_tipo') == 'f'){
                $this->obj->cpf          = $this->input->post('ed_cpf');
            }
            else{
                $this->obj->cnpj         = $this->input->post('ed_cnpj');
                $this->obj->razao_social = $this->input->post('ed_razao_social');
            }*/
            //die($this->obj->telefone);

            if ( $this->validate_cliente() )
                if ( $idCliente  = $this->obj->incluir() ) {

                    $this->obj->newTelefone($idCliente,$telefones);
                    $this->session->set_flashdata('msg', 'Cliente cadastrado com sucesso');
                    redirect(site_url('clientes/atualizar/' . $idCliente));
                
                } else {

                    $this->session->set_flashdata('msgBg', '#990000');
                    $this->session->set_flashdata('msg', 'Erro ao tentar cadastrar');
                    redirect(site_url('clientes/incluir/'));

                }
        }/*FIM DO POST*/

        $var = array(
            'js'      => 'clientes_form.js|jquery.maskedinput.min.js|jquery-ui.min.js',
            'css'     => 'jquery-ui.min.css',
            'title'   => 'Novo Cliente'
        );

        $this->load->view('clientes/form', $var);
    }

    public function atualizar($idCliente, $pg = 0) {
        $quantidade = 10;

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $this->obj->nome        = $this->input->post('ed_nome');
            $this->obj->cpf         = $this->input->post('ed_cpf');
            $this->obj->rg         = $this->input->post('ed_rg');
            $this->obj->dataNascimento  = $this->input->post('ed_dataNascimento');
            $this->obj->nomeMae         = $this->input->post('ed_nomeMae');
            $this->obj->nomePai         = $this->input->post('ed_nomePai');
            $this->obj->email       = $this->input->post('ed_email');
            $this->obj->telefone    = $this->input->post('ed_telefone');
            $this->obj->telefone2   = $this->input->post('ed_celular');
            $this->obj->cep         = $this->input->post('ed_cep');
            $this->obj->estado      = $this->input->post('ed_uf');
            $this->obj->cidade      = $this->input->post('ed_cidade');
            $this->obj->bairro      = $this->input->post('ed_bairro');
            $this->obj->logradouro  = $this->input->post('ed_endereco');
            $this->obj->numero      = $this->input->post('ed_numero');
            $this->obj->complemento = $this->input->post('ed_complemento');
            $this->obj->tipo        = ($this->input->post('ed_tipo') == 'j')?'t':'f';
            $this->obj->idCliente   = $idCliente;

            $qtdTelefone = $this->input->post('ed_total_tel');
            $i = 0;
            while ($qtdTelefone > $i) {
                if($this->input->post('ed_telefone'.$i)){
                    $telefones[] = $this->input->post('ed_telefone'.$i);
                    
                }$i++;
            }

            /*PESSOA JURIDICA OU PESSOA FISICA*/
            /*if($this->input->post('ed_tipo') == 'f'){
                $this->obj->cpf          = $this->input->post('ed_cpf');
            }
            else{
                $this->obj->cnpj         = $this->input->post('ed_cnpj');
                $this->obj->razao_social = $this->input->post('ed_razao_social');
            }*/

            if($this->validate_cliente() ){
                $this->obj->updateClient();
                $this->obj->updateTelefone($idCliente,$telefones);
            }
        }

        $var = array(
            'js'        => 'clientes_form.js|jquery.maskedinput.min.js|jquery-ui.min.js',
            'css'       => 'jquery-ui.min.css',
            'title'     => 'Atualizar Cliente',
            'cliente'    => $this->obj->getClient($idCliente),
            'telefones' => $this->obj->telefones
        );

        // Paginação para as anotações do cliente selecionado
        $config = array(
            'base_url'    => site_url('clientes/update_cliente/' . $idCliente) . '/',
            'per_page'    => $quantidade,
            'next_link'   => 'próxima',
            'prev_link'   => 'anterior',
            'last_link'   => 'última',
            'first_link'  => 'primeira',
            'uri_segment' => 4 
            
        );

        $this->pagination->initialize($config);

        $this->load->view('clientes/form', $var);
    }    

    public function filtrar($posicao = 0) {
        $quantidade = 10;

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $this->obj->filter = $this->input->post('ed_filtro');
        }

        $var = array(
            'js'      => 'clients_list.js',
            'clients' => $this->obj->listar($posicao, $quantidade),
            'count'   => $this->obj->clientCount,            
            'busca'   => $this->obj->filter
        );
        
         $config = array(
            'base_url'   => site_url('clients/index') . '/',
            'total_rows' => $this->obj->clientCount,
            'per_page'   => $quantidade,
            'next_link'  => 'próxima',
            'prev_link'  => 'anterior',
            'last_link'  => 'última',
            'first_link' => 'primeira'
        );
        
        $this->pagination->initialize(); 

        $this->load->view('clientes/list', $var);
    }

    public function verefica_cpf(){     
        echo json_encode($this->obj->vereficaCpf());
    }


    

    public function validate_cliente() {
       
        if ( $this->input->post('ed_tipo') == 'f' ) {


            $config = array(            
              
                array(
                    'field' => 'ed_nome',
                    'label' => 'Nome',
                    'rules' => 'trim|required|min_length[5]|max_length[100]'
                ),
                array(
                    'field' => 'ed_cpf',
                    'label' => 'CPF',
                    'rules' => 'exact_length[14]|valid_CPF'
                ),array(
                    'field' => 'ed_rg',
                    'label' => 'RG',
                    'rules' => 'number'
                ),array(
                    'field' => 'ed_dataNascimento',
                    'label' => 'Data de Nascimento',
                    'rules' => 'trim'
                ),array(
                    'field' => 'ed_nomeMae',
                    'label' => 'Nome da Mãe',
                    'rules' => 'trim'
                ),array(
                    'field' => 'ed_nomePai',
                    'label' => 'Nome do Pai',
                    'rules' => 'trim'
                ),array(
                    'field' => 'ed_email',
                    'label' => 'E-mail',
                    'rules' => 'valid_email'
                ),array(
                    'field' => 'ed_telefone0',
                    'label' => 'Telefone 1',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'ed_cep',
                    'label' => 'CEP',
                    'rules' => 'required|exact_length[9]'
                ),
                array(
                    'field' => 'ed_uf',
                    'label' => 'Estado',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'ed_cidade',
                    'label' => 'Cidade',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'ed_bairro',
                    'label' => 'Bairro',
                    'rules' => 'trim|required|max_length[100]'
                ),
                array(
                    'field' => 'ed_endereco',
                    'label' => 'Endereço',
                    'rules' => 'trim|required|max_length[250]'
                ),
                array(
                    'field' => 'ed_numero',
                    'label' => 'Número',
                    'rules' => 'trim'
                ),
                array(
                    'field' => 'ed_complemento',
                    'label' => 'Complemento',
                    'rules' => 'trim|max_length[50]'
                )
            );
	}else
            $config = array(
                array(
                    'field' => 'ed_cod_cliente',
                    'label' => 'Código do cliente',
                    'rules' => 'trim|required|integer'
                ),
                array(
                    'field' => 'ed_tipo',
                    'label' => 'Tipo',
                    'rules' => 'required|exact_length[1]'
                ),
                array(
                    'field' => 'ed_nome',
                    'label' => 'Nome',
                    'rules' => 'trim|required|min_length[5]|max_length[100]'
                ),
                array(
                    'field' => 'ed_cnpj',
                    'label' => 'CNPJ',
                    'rules' => 'required|exact_length[18]|valid_CNPJ'
                ),
                array(
                    'field' => 'ed_razao_social',
                    'label' => 'Razão Social',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'ed_email',
                    'label' => 'Email',
                    'rules' => 'trim|valid_email|max_length[100]'
                ),
                array(
                    'field' => 'ed_telefone',
                    'label' => 'Telefone',
                    'rules' => 'required|exact_length[14]'
                ),
                array(
                    'field' => 'ed_celular',
                    'label' => 'Celular',
                    'rules' => 'exact_length[14]'
                ),
                array(
                    'field' => 'ed_cep',
                    'label' => 'CEP',
                    'rules' => 'required|exact_length[9]'
                ),
                array(
                    'field' => 'ed_cidade',
                    'label' => 'Cidade',
                    'rules' => 'required|integer'
                ),
                array(
                    'field' => 'ed_bairro',
                    'label' => 'Bairro',
                    'rules' => 'trim|required|max_length[100]'
                ),
                array(
                    'field' => 'ed_endereco',
                    'label' => 'Endereço',
                    'rules' => 'trim|required|max_length[250]'
                ),
                array(
                    'field' => 'ed_numero',
                    'label' => 'Número',
                    'rules' => 'trim|required|integer'
                ),
                array(
                    'field' => 'ed_complemento',
                    'label' => 'Complemento',
                    'rules' => 'trim|max_length[50]'
                )
            );


        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters("<p><span class='label label-danger'>", "</span></p>");

        return $this->form_validation->run();
    }
}
