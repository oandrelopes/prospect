<?php 

class Permissoes extends CI_Controller { 

	function __construct() { 
		parent::__construct(); 
	} 

	function index() { 
		$this->auth->check_logged( $this->router->class , $this->router->method );
	} 
} 

