 <?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_file extends CI_Controller {

  	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library( array('form_validation', 'permissoes', 'upload') );

        /*SEGURANCA*/
        $this->idEmpresa = $this->session->userdata('id_empresa');
		$result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );

        if ( !$result || !$this->idEmpresa ) {
            redirect(base_url().'login', 'refresh');
        }
        /*SEGURANCA*/
	}


	function upload_it() {
		//load the helper
		$this->load->helper('form');

		$caminho_upload = 'public/uploads/'.$this->idEmpresa.'/';

		if ( !file_exists( $caminho_upload ) ) {
			mkdir($caminho_upload, 0777, true);
		}
		chmod($caminho_upload, 0777);

		//Configure
		//set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
		$config['upload_path'] = $caminho_upload;
		// set the filter image types
		$config['allowed_types'] = 'gif|jpg|png|pdf';
		//load the upload library
	  	$this->upload->initialize($config);

		$data['upload_data'] = '';

		//if not successful, set the error message
		if ( !$this->upload->do_multi_upload('userfile') ) {
			$data = array('msg' => $this->upload->display_errors());
		} else { //else, set the success message
			$data = array('msg' => "Upload success! " . $this->upload->display_errors() );
			$data['upload_data'] = $this->upload->data();
		}

		//load the view/upload.php
		$this->load->view('upload', $data);
		
	}

}