<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        // this is your constructor
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('permissoes');
    }

	function index() {
	    if( $this->session->userdata('isLoggedIn') ) {
	        redirect('/inicio');
        } else {
	        $this->show_login(false);
	    }
	}

	function show_login( $show_error = false ) {
      $data['error'] = $show_error;
	    $data['title'] = 'Projeto Prospect';
	    $this->load->view('login',$data);
	}

  	function login_user() {
      	// Create an instance of the user model
      	$this->load->model('login_model');

      	// Grab the email and password from the form POST
      	$email = $this->input->post('email');
      	$pass  = $this->input->post('password');

      	//Ensure values exist for email and pass, and validate the user's credentials
      	if( $email && $pass && $this->login_model->validate_user($email,$pass)) {
          	// If the user is valid, redirect to the main view
          	redirect('/inicio');
      	} else {
          	// Otherwise show the login screen with an error message.
          	$this->show_login(true);
      	}
  	}

    function logout(){
        $this->permissoes->logout();
        redirect('/');
    }

    function password($p){
        echo $this->encrypt->encode($p);
    }
}