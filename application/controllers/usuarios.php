<?php

class Usuarios extends CI_Controller 
{

    function __construct() {
        // this is your constructor  
        parent::__construct();   
        $this->load->helper( array('url', 'form', 'default') );
        $this->load->library( array('form_validation', 'permissoes') );
        $this->load->model('usuarios_model', 'obj');     
        $result = $this->permissoes->check_logged( $this->router->fetch_class() , $this->router->fetch_method() );
        $this->obj->idEmpresa = $this->session->userdata('id_empresa');

        if ( !$result || !$this->obj->idEmpresa )
            redirect(base_url().'login', 'refresh');
    }    

    function index() {

    	$var = array(
            'usuarios'           => $this->obj->listar(),
            'js'              => 'user.js'	       
        );

        $this->load->view('usuarios/list', $var);
	    
    }

    function incluir() {

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

            $this->obj->nome         = $this->input->post('ed_nome');
            $this->obj->email        = $this->input->post('ed_email');
            $this->obj->senha        = $this->input->post('ed_senha');
            $this->obj->ativo        = ($this->input->post('ed_ativo') == 'on')?'1':'0';
	        $this->obj->tipo_usuario = $this->input->post('ed_tipo_usuario');
            
            if ( $this->validate_user() ) {
                if ( $this->obj->incluir() )
                    redirect(site_url('usuarios'));
            }
        }
	
        $var = array(
            'css'        => 'user.css',
            'js'         => 'user.js'
        );

        $this->load->view('usuarios/form', $var);
    }

    function validate_user() {

        $config = array(
            array(
                'field' => 'ed_nome',
                'label' => 'Nome',
                'rules' => 'required|min_length[3]|max_length[100]|max_length[100]'
            ),
            array(
                'field' => 'ed_email',
                'label' => 'Email',
                'rules' => 'required|valid_email|max_length[100]'
            ),
            array(
                'field' => 'ed_senha',
                'label' => 'Senha',
                'rules' => 'required|min_length[6]|matches[ed_csenha]'
            ),
            array(
                'field' => 'ed_csenha',
                'label' => 'Confirmar senha',
                'rules' => 'required|min_length[6]'
            )
        );

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<div class="ui red pointing above ui label">', '</div>');

        return $this->form_validation->run() == true;
    }

    function update_password(){
        $msg = '';       
        
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $this->obj->asenha = $this->input->post('ed_asenha');
            $this->obj->nsenha = $this->input->post('ed_nsenha');
            $this->obj->rsenha = $this->input->post('ed_rsenha');
	
	 
        if(strcmp($this->obj->nsenha, $this->obj->rsenha) === 0)
			if(strlen($this->obj->nsenha) >= 6)
	                   if($this->obj->updatePassword() )
	                       $msg = 'Senha alterada com sucesso.';
	                   else
	                       $msg = 'Senha atual incorreta!';
			else
			   $msg = 'A nova senha deve conter 6 caractéres ou mais!';
	            else
	                $msg = 'As senhas n&atilde;o conferem!';
	        }
	    
        $this->load->view('usuarios/update_password', array('msg' => $msg));
    }

      // Método que lista todos os usuários on-line
    function online(){
		 // Apenas o usuário de código "1" pode gerenciar usuários
        //if($this->usuario->get('cod_tipo') != 1)
        	//redirect(site_url('inicio'));

        if(!$this->privilegio()){
            redirect(site_url());
        }
	$help_valor = 'useronline';
    	$var = array(
            'usuarios' => $this->obj->onlineList(),
            'title' => 'Usu&aacute;rios On-line',
	    'ajuda' => $help_valor
    	);

    	$this->load->view('usuarios/list_online', $var);
    }

    function usuario_logado() {
        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $toDescrypt = $this->input->post('session_post');
            $toDescrypt = urldecode($toDescrypt);
            $toDescrypt = $this->encrypt->decode($toDescrypt);
            $toDescrypt = unserialize($toDescrypt);

            $this->obj->id_sessao = $toDescrypt['session_id'];
            $resultado = $this->obj->pegar_user_online();
            if ( $resultado ) {
                $resultado = unserialize( $resultado[0]->user_data );
                echo json_encode( $resultado );
            } else {
                echo json_encode( array('resposta'=> null) );
            }
        }
    }

     function logoff($cod_user){
        // if(!$this->privilegio()){
        //     redirect(site_url());
        // }

        $this->obj->user_id = $cod_user;
        if($this->obj->logOff())
            redirect('usuarios/online');
	}

      function validate_update_password(){
        $config = array(
	    array(
                'field' => 'ed_asenha',
                'label' => 'Senha Antiga',
                'rules' => 'required'
            ),
            array(
                'field' => 'ed_nsenha',
                'label' => 'Nova Senha',
                'rules' => 'required|min_length[6]')
	    );

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<div class="ui red pointing above ui label">', '</div>');

        return $this->form_validation->run() == true;
    }


    function atualizar($cod_user) {

        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $this->obj->nome        = $this->input->post('ed_nome');
            $this->obj->email       = $this->input->post('ed_email');
            $this->obj->cod_setor   = $this->input->post('ed_cod_setor');
            $this->obj->cod_usuario = $cod_user;
            $this->obj->ativo       = ($this->input->post('ed_ativo') == 'on')?'1':'0';
            $this->obj->tipo_usuario  = $this->input->post('ed_tipo_usuario');
            
            if($this->obj->atualizar())
                redirect(site_url('usuarios'));
        }

        $var = array(
            'permissoes'  => $this->listar_permissoes($cod_user),
            'usuarios'    => $this->obj->getUser($cod_user),
            'js'          => 'user.js'	    
        );
        
        $this->load->view('usuarios/form', $var);
    }

    function drop_user($cod_usuario){
        $this->obj->cod_usuario = $cod_usuario;

        if($this->obj->dropUser())
            redirect(site_url('usuarios'));
    }

    function listar_permissoes($cod_user) {
        $result = $this->permissoes->check_logged( 'usuarios' , 'listar_permissoes' );
        if ( $result )
            return $this->permissoes->listar_permissoes($cod_user);
        else
            return false;
    }

    function salvar_permissoes() {
        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            $id_user = $this->input->post('id_user');
            $id_metodo = $this->input->post('id_metodo');
            $ativo = $this->input->post('ativo');
            
            $this->permissoes->salvar_permissoes( $id_user, $id_metodo, $ativo );
        }
    }
}
