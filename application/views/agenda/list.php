<?php $this->load->view('cabecalho'); ?>
    <h4>Agenda</h4>
    
    <div class="ui divider"></div>

    
		
		<div class="ui top attached tabular menu">
			<?php foreach ($agenda as $key => $cat ) : ?>
				<?php if ( !isset($cat_atual) || $cat_atual != $cat->id_categoria ) : ?>
					<?php 
						$cat_atual = $cat->id_categoria; 
						$array_categoria[] = $cat->id_categoria; 
					?>
		  			<a class="<?=$key==0?'active':'';?> item" data-tab="<?=$cat->id_categoria;?>"><?=$cat->id_categoria;?></a>
	  			<?php  endif; ?>
	  		<?php endforeach; ?>
		</div>
		
		<?php foreach ( $array_categoria as $key => $id_cat ) : ?>
		
			<div class="ui tab segment <?=$key==0?'active':'';?>" data-tab="<?=$id_cat;?>">

			    <table class="ui table segment">
			        <thead>
			            <tr>
			                <th>Empresa</th>
			                <th>Usuário</th>
			                <th>Data Inicial</th>
			                <th>Data Final</th>
			                <th>Status</th>
			            </tr>
			        </thead>
			        <tbody>
			            <? 
			            	foreach ( $agenda as $i => $a ) : 
			            		if ( $id_cat === $a->id_categoria ) :
			            ?>
					            <tr>
					                <td><?=$a->id_empresa?></td>
					                <td><?=$a->id_cliente?></td>
					                <td><?=$a->data_inicial?></td>
					                <td><?=$a->data_final?></td>

						            <td>
							            <div class="ui toggle checkbox check_status_agenda" <? echo ( $a->status == null ) ? "title='Pendente'" : "title='Aprovado'" ?> data-id-agenda="<?=$a->id;?>" >
							                <input <? echo ( $a->status == 1 ) ? "checked='checked'" : "" ?> type="checkbox" class="checkbox" id="check_status_agenda" name="check_status_agenda" />
							                <label></label>
							            </div>
						            </td>
					            </tr>
			            <?
			            		endif; 
			            	endforeach;
		            	?>
			        </tbody>
			        <tfoot>
			            <tr>
			                <th>
			                    <!-- <a href="<?#=site_url('agenda/incluir')?>" class="btn btn-success"><div class="ui blue labeled icon button"><i class="user icon"></i>Cadastrar Agenda</div></a> -->
			                </th>
			                <th></th>
			                <th></th>
			                <th></th>
			                <th></th>
			            </tr>
			        </tfoot>
			    </table>

		    </div>

		<? endforeach; ?>

<?php $this->load->view('rodape'); ?>