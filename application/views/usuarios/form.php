<? $this->load->view('cabecalho'); ?>

    <h4>Novo Usuário</h4>
    
    <div class="ui divider"></div>
    
    <form action="" method="post" class="ui loading form segment">
    
        <div class="field">
            <label>Nome</label>
            <div class="controls">
                <input value="<?=set_value('ed_nome', @$usuarios->nome)?>" type="text" name="ed_nome" id="ed_nome" class="input-block-level"/>
                <?=form_error('ed_nome')?>
            </div>
        </div>
        
        <div class="field">
            <label>E-mail</label>
            <div class="controls">
                <input size="40" maxlength="100" value="<?=set_value('ed_email', @$usuarios->email)?>" type="text" name="ed_email" id="ed_email" class="input-block-level"/>
                <?=form_error('ed_email')?>
            </div>
        </div>
        
        <? if(@$usuarios->nome == ''):?>
        <div class="field">
            <label>Senha</label>
            <div class="controls">
                 <input size="40" type="password" name="ed_senha" id="ed_senha" class="input-block-level"/>
                 <?=form_error('ed_senha')?>
            </div>
        </div>
        
        <div class="field">
            <label>Digite a Senha novamente</label>
            <div class="controls">
                <input size="40" type="password" name="ed_csenha" id="ed_csenha" class="input-block-level"/>
                <?=form_error('ed_csenha')?>
            </div>
        </div>
        
        <? endif;?>
        
        <div class="inline field">
            <div class="ui slider checkbox">
                <input <? if ( isset($usuarios) && $usuarios->ativo==1 ) { echo "checked='checked'"; } elseif ( isset($usuarios) && $usuarios->ativo==0 ){echo "";} else {echo "checked='checked'";}?> type="checkbox" class="checkbox" id="ed_ativo" name="ed_ativo" />
                <label>Usuário Ativo</label>
            </div>
        </div>

        <!-- <div class="field">
            <div class="ui fluid selection dropdown">
                <div class="text">Nível do Usuário</div>
                    <i class="dropdown icon"></i>
                    <input type="hidden" name="ed_tipo_usuario" value="<?=@$usuarios->id_permissao?>">
                    <div class="menu">
                        <div class="item" data-value="3">Usuário Comum</div>
                        <div class="item" data-value="2">Usuário</div>
                        <div class="item" data-value="1">Administrador</div>
                    </div>
            </div>
        </div> -->
        
        <?php if ( isset($usuarios->id) && $usuarios->id != "" && isset($permissoes) && $permissoes ) : ?>
        <table class="ui table segment table-permissao">
            <thead>
                <tr>
                    <th>Classe</th>
                    <th>Metodo</th>
                    <th>Apelido</th>
                    <th>Permissão</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($permissoes as $key => $value) : ?>
                    <tr>
                        <th><?=$value->classe;?></th>
                        <th><?=$value->metodo;?></th>
                        <th><?=$value->apelido;?></th>
                        <th>                            
                            <div class="ui toggle checkbox checkbox-permissao" data-iduser="<?=$usuarios->id;?>" data-idmetodo="<?=$value->id;?>">
                                <input <? if ( isset($value->permissao) && $value->permissao==1 ) { echo "checked='checked'"; } else {echo "";} ?> type="checkbox" class="checkbox" name="ed_permissao" />
                                <label></label>
                            </div>                                          
                        </th>               
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot></tfoot>
        </table>
        
        <div class="mensagens-permissoes">
            <div class="ui hidden compact small green message">Permissão Salva</div>
            <div class="ui hidden compact small red message">Erro ao salvar</div>
        </div>

        <?php endif; ?>

        <div class="form-actions">
            <div class="ui buttons">
                <a href="<?=site_url('usuarios')?>" class="ui button cancelar">Cancel</a>
                <div class="or"></div>
                <button type="submit" class="ui positive button salvar">Salvar</button>
            </div>          
        </div>

    </form>
        

<? $this->load->view('rodape'); ?>