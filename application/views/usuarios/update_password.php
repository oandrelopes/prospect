<? $this->load->view('cabecalho'); ?>
    <h4>Alterar senha</h4>
	<div class="ui divider"></div>
    <form action="" method="post" class="ui loading form segment">

    	<div class="field">
	    	<label>Senha Antiga</label>
	    	<div class="controls">
		    	<input size="40" maxlength="100" autocomplete="off" type="password" name="ed_asenha" id="ed_asenha" class="input-block-level"/>
		    </div>
    	</div>
    	
    	<div class="field">
	    	<label>Nova Senha</label>
	    	<div class="controls">
		    	<input size="40" maxlength="100" autocomplete="off" type="password" name="ed_nsenha" id="ed_nsenha" class="input-block-level"/>
		    </div>
    	</div>
    	
    	<div class="field">
	    	<label>Repita a Nova Senha</label>
	    	<div class="controls">
		    	<input size="40" maxlength="100" autocomplete="off" type="password" name="ed_rsenha" id="ed_rsenha" class="input-block-level"/>
	            <p class="error"><?=(@$msg != null)?$msg:''?></p>
		    </div>
    	</div>

	    <div class="form-actions">
			<div class="ui buttons">
			  	<a href="<?=site_url()?>" class="ui button cancelar">Cancel</a>
			  	<div class="or"></div>
			  	<button type="submit" class="ui positive button salvar">Salvar</button>
			</div>			
		</div>

    </form>

<? $this->load->view('rodape'); ?>