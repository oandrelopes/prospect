<? $this->load->view('cabecalho'); ?>

    <h4>Usuários</h4>

    <table class="ui table segment">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Ativo</th>
            </tr>
        </thead>
        <tbody>
            <? foreach($usuarios as $i => $u):?>
            <tr>
                <td><a href="<?=site_url('usuarios/atualizar/' . $u->id)?>"><?=$u->nome?></a></td>
                <td><?=$u->email?></td>
                <td><?=$u->ativo?></td>
            </tr>
            <? endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th>
                    <a href="<?=site_url('usuarios/incluir')?>" class="btn btn-success"><div class="ui blue labeled icon button"><i class="user icon"></i>Cadastrar Usuário</div></a>
                </th>
                <th></th>
                <th><?=sizeof($usuarios)?></span> Usuários Cadastrados</th>
            </tr>
        </tfoot>
    </table>

<? $this->load->view('rodape'); ?>