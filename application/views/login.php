<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title><?=$title;?></title>
    
    <link rel="stylesheet" type="text/css" href="<?=site_url('/public/semantic/css/semantic.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('/public/css/style.css');?>">

</head>
<body>
    <div class="box-login container">
        <div class="ui one column middle aligned relaxed grid basic segment ">
            <div class="column">
                <?php echo form_open('login/login_user'); ?>
                    <div class="ui form segment tertiary">
                        <div class="field">
                            <label>Username</label>
                            <div class="ui left labeled icon input <?=($error?' error':'');?> ">
                                <?php echo form_input('email'); ?>
                                <i class="user icon"></i>
                                <div class="ui corner label">
                                    <i class="asterisk icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label>Password</label>
                            <div class="ui left labeled icon input <?=($error?' error':'');?> ">
                                <?php echo form_password('password'); ?>
                                <i class="lock icon"></i>
                                <div class="ui corner label">
                                    <i class="asterisk icon"></i>
                                </div>
                            </div>
                        </div>
                        <?php 
                            $attributes = array('type' => 'submit', 'value' => 'login', 'class' => 'ui blue submit button');
                            echo form_submit( $attributes ); 
                        ?>
                    </div><!-- ui form segment tertiary -->
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <script src="<?=site_url('/public/js/jquery-2.1.0.min.js');?>"></script>
    <script src="<?=site_url('/public/semantic/javascript/semantic.js');?>"></script>
</body>
</html>
