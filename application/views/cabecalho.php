<!DOCTYPE html>
<html>
<head>    
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properities -->
    <title>Prospect</title>

    <!--<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700' rel='stylesheet' type='text/css'>-->
    <link rel="stylesheet" type="text/css" href="<?=site_url('/public/semantic/css/semantic.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('/public/css/style.css');?>">
	<? if ( isset($css) && $css ) echo css('/public/css', $css) ?>

    <script>
        //variavel global;
        var site_url =  '<?=site_url();?>';
    </script>
	
    
</head>
<body>
    
    <div class="ui tiered menu inverted">
        <div class="container">
            <div class="ui dropdown item">
                <i class="calendar icon"></i> Prospect
                <div class="menu">
                    <a class="item" href="<?=site_url();?>" ><i class="list icon"></i> Listar prospect</a>
                    <a class="item" href="<?=site_url('inicio/incluir');?>" ><i class="add icon"></i> Cadastrar prospect</a>                    
                </div>
            </div>

            <a class="item" href="<?=site_url('clientes');?>"><i class="mail icon"></i> Clientes</a>

            <div class="ui dropdown item">
                <i class="user icon"></i> Usuarios
                <div class="menu">
                    <a class="item" href="<?=site_url('usuarios');?>" ><i class="users icon"></i> Listar Usuarios</a>
                    <a class="item" href="<?=site_url('usuarios/incluir');?>" ><i class="add icon"></i> Cadastrar Usuarios</a>                    
                </div>
            </div>
            
            <!-- <a class="item" href="<?=site_url('empresa');?>"><i class="building icon"></i> Empresa</a> -->

            <div class="right menu">
                <!-- <div class="item">
                    <div class="ui icon input">
                        <input type="text" placeholder="Search...">
                        <i class="search link icon"></i>
                    </div>
                </div> -->
                <div class="ui dropdown item">
                    <i class="icon settings"></i>
                    <div class="menu">
                        <a class="item" href="<?=site_url('metodos');?>"><i class="list icon"></i> Métodos</a>
                        <!-- <a class="item"><i class="globe icon"></i> Choose Language</a> -->
                        <a class="item" href="<?=site_url('usuarios/update_password');?>"><i class="settings icon"></i> Alterar Minha senha</a>
                        <a class="item" href="<?=site_url('login/logout');?>"> Sair</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main container">
