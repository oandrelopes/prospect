<script>
	//variaveis globais;
    var site_url     = '<?=site_url(); ?>';
    var slug_empresa = '<?=$empresa->slug; ?>';
    var endereco_empresa = '<?=$empresa->endereco; ?>';

$(function() {
 	
 	if ( endereco_empresa )
	 	$.ajax({
	    	url: 'http://maps.google.com/maps/api/geocode/json',
	    	//type: 'default GET (Other values: POST)',
	    	dataType: 'json',
	    	data: {address: endereco_empresa, sensor: false },
	    })
	    .success( function(data){
	    	console.log( data.results[0].geometry.location );
	    	var latitude = data.results[0].geometry.location.lat;
	    	var longitude = data.results[0].geometry.location.lng;	

			map = new GMaps({
				el: '#map',
				zoom: 16,
				lat: latitude,
				lng: longitude,
				zoomControl : false,
				panControl : false,
				streetViewControl : false,
				mapTypeControl: false,
				overviewMapControl: false
			});

			map.addMarker({
				lat: latitude,
				lng: longitude,
				title: 'Marker with InfoWindow',
				infoWindow: {
				  content: '<p>'+endereco_empresa+'</p>'
				}
			});
	    })
	    .done(function() {
	    	console.log("success");
	    })
	    .fail(function() {
	    	console.log("error");
	    })
	    .always(function() {
	    	console.log("complete");
	    });

});    


</script>