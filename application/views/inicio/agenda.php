<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link href="http://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet" type="text/css">
	
<link href="<?=site_url('/public/deixeagendado/css/main.css');?>" rel="stylesheet" />
<link href="<?=site_url('/public/deixeagendado/css/fullcalendar.print.css');?>" rel="stylesheet" media="print" />


<script src="<?=site_url('/public/deixeagendado/js/jquery.min.js');?>"></script>
<script src="<?=site_url('/public/deixeagendado/js/jquery-ui.custom.min.js');?>"></script>
<? $this->load->view('inicio/scripts'); ?>

<script src="<?=site_url('/public/deixeagendado/js/moment.min.js');?>"></script>
<script src="<?=site_url('/public/deixeagendado/js/fullcalendar.min.js');?>"></script>
<script src="<?=site_url('/public/deixeagendado/js/lang-all.js');?>"></script>


<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<?=site_url('/public/deixeagendado/js/gmaps.js');?>"></script>
<script src="<?=site_url('/public/deixeagendado/js/gcal.js');?>"></script>
<script src="<?=site_url('/public/deixeagendado/js/semantic.min.js');?>"></script>
<script src="<?=site_url('/public/deixeagendado/js/jquery.cycle.all.js');?>"></script>

<script src="<?=site_url('/public/deixeagendado/js/script.js');?>"></script>

</head>
<body>
	<header id="header">
		<h4 id="logo">deixeagendado</h4>
		<nav id="menu">
			<ul>
				<li><a href="#mod-calendar">Calendário</a></li>
				<li><a href="#mod-localizacao">Localização</a></li>
				<li><a href="#mod-contato">Fotos</a></li>
				<li><a href="#mod-contato">Contato</a></li>
			</ul>
		</nav>
	</header>
	
	
	<section id="mod-calendar">
		<div id="script-warning">
			<code>php/get-events.php</code> must be running.
		</div>
		<div id="loading">loading...</div>
		
		<aside id="aside">
			<h3>Agendas</h3>
			<div class="ui form">
			  	<div class="inline field">
				    <div class="ui checkbox">
				      	<input type="checkbox">
				      	<label>Marcelo Azevedo</label>
				    </div>
			    
				    <div class="ui checkbox">
				      	<input type="checkbox">
				      	<label>Daniel Telles</label>
				    </div>
			  	</div>

			</div>
		</aside>
		<div id="calendar"></div>
		
	</section>
	
	<?php if ( isset($empresa->sobre) && $empresa->sobre ) : ?>
		<section id="dados-empresa">
			<div class="center">
				<p><?php echo $empresa->sobre; ?></p>
			</div>
		</section>
	<?php endif; ?>
	
	<?php if ( isset($empresa->endereco) && $empresa->endereco ) : ?>
		<section id="mod-localizacao">
			<div class="tit">
				<h2>Localização</h2>
			</div>
			<div id="map"></div>
		</section>
	<?php endif; ?>
	
	<section id="mod-galeria">
		<div class="center">
			<h2>Galeria de Imagens</h2>
			<ul id="nav"></ul>
			<section id="slideshow">
				<img src="images/galeria/ex01.jpg" alt="" width="550" height="400">
				<img src="images/galeria/ex02.jpg" alt="" width="550" height="400">
				<img src="images/galeria/ex03.jpg" alt="" width="550" height="400">
				<img src="images/galeria/ex04.jpg" alt="" width="550" height="400">
				<img src="images/galeria/ex05.jpg" alt="" width="550" height="400">
				<img src="images/galeria/ex06.jpg" alt="" width="550" height="400">
			</section>
		</div>
	</section>
	
	<section id="mod-contato">
		<div class="center">
			<h2>Contato</h2>
			<form action="">
				<div class="ui error form segment">
					
					<div class="field">
						<input placeholder="Nome:" type="text" name="name">
					</div>
					
					<div class="field">
						<input placeholder="E-mail:" type="email" name="email">
					</div>
					
					<div class="field">
						<input placeholder="Telefone:" type="tel" name="tel">
					</div>
					
					<div class="field">
						<textarea placeholder="Mensagem:" name="msg"></textarea>
					</div>

					<div class="ui submit button">enviar</div>
					
				</div>
			</form>
			
			<aside class="aside-contato">
				<p>Preencha o formulário ao lado e entre em contato conosco, responderemos o quanto antes. Desde já agradecemos o seu contato.</p>
				<h3>Nossas Redes Sociais</h3>
				<div class="redes-cont">
					
					<?php if ( isset($empresa->facebook) && $empresa->facebook ) : ?>
						<a href="<?=$empresa->facebook?>" target="_blank" >
							<i class="circular inverted facebook icon"></i>
						</a>
					<?php endif; ?>

					<?php if ( isset($empresa->gplus) && $empresa->gplus ) : ?>
						<a href="<?=$empresa->gplus?>" target="_blank" >
							<i class="circular inverted google plus icon"></i>
						</a>
					<?php endif; ?>
					
					<?php if ( isset($empresa->twitter) && $empresa->twitter ) : ?>
						<a href="<?=$empresa->twitter?>" target="_blank" >
							<i class="circular inverted twitter icon"></i>
						</a>
					<?php endif; ?>

					<?php if ( isset($empresa->youtube) && $empresa->youtube ) : ?>
						<a href="<?=$empresa->youtube?>" target="_blank" >
							<i class="circular inverted youtube icon"></i>
						</a>
					<?php endif; ?>

				</div>
			</aside>
		</div>
		
	</section>
	
	<footer id="footer">
		<h1 id="logo-footer">deixeagendado</h1>
	</footer>

	

</body>
</html>
