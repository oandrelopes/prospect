<? $this->load->view('cabecalho'); ?>

    <h4><?=$title;?></h4>
    
    <div class="ui divider"></div>
    
    <form action="" method="post" class="ui loading form segment">
    
        <div class="field">
            <label>Nome</label>
            <div class="controls">
                <input value="<?=set_value('p_nome', @$prospect->nome)?>" type="text" name="p_nome" id="p_nome" class="input-block-level"/>
                <?=form_error('p_nome')?>
            </div>
        </div>
        
        <div class="field">
            <label>Setor</label>
            <div class="controls">
                <input size="40" maxlength="100" value="<?=set_value('p_setor', @$prospect->setor)?>" type="text" name="p_setor" id="p_setor" class="input-block-level"/>
                <?=form_error('p_setor')?>
            </div>
        </div>

        <div class="field">
            <label>Contato</label>
            <div class="controls">
                <input size="40" maxlength="100" value="<?=set_value('p_contato', @$prospect->contato)?>" type="text" name="p_contato" id="p_contato" class="input-block-level"/>
                <?=form_error('p_contato')?>
            </div>
        </div>

        <div class="field">
            <label>Data da Reunião</label>
            <div class="controls">
                <input size="40" maxlength="100" value="<?=set_value('p_data_reuniao', @$prospect->data_reuniao)?>" type="text" name="p_data_reuniao" id="p_data_reuniao" class="input-block-level calendario"/>
                <?=form_error('p_data_reuniao')?>
            </div>
        </div>

        <div class="field">
            <label>Data para Prazo Final</label>
            <div class="controls">
                <input size="40" maxlength="100" value="<?=set_value('p_data_final', @$prospect->data_final)?>" type="text" name="p_data_final" id="p_data_final" class="input-block-level calendario"/>
                <?=form_error('p_data_final')?>
            </div>
        </div>

        <div class="field">
            <label for="">Status</label>
            <div class="ui fluid selection dropdown">
                <div class="text">Selecione</div>
                    <i class="dropdown icon"></i>
                    <input type="hidden" name="p_status" value="<?=@$prospect->status?>">
                    <div class="menu">
                        <div class="item" data-value="1">Adjudicou</div>
                        <div class="item" data-value="2">Não Adjudicou</div>
                    </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="ui buttons">
                <a href="<?=site_url()?>" class="ui button cancelar">Cancel</a>
                <div class="or"></div>
                <button type="submit" class="ui positive button salvar">Salvar</button>
            </div>
        </div>

    </form>


<? $this->load->view('rodape'); ?>