<? $this->load->view('cabecalho'); ?>

    <h4><?=$title;?></h4>

    <table class="ui table segment">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Setor</th>
                <th>Contato</th>
                <th>Status Ação</th>
                <th>Próxima Ação</th>
                <th>Data Cadastro</th>
            </tr>
        </thead>
        <tbody>
            <? foreach($prospect as $k => $p):?>
            <tr>
                <td><a title="Editar" href="<?=site_url('atualizar/' . $p->id)?>"><i class="edit icon"></i> <?=$p->nome?> </a></td>
                <td><?=$p->setor;?></td>
                <td><?=$p->contato?></td>
                <td><? $return = cp($p->data_cadastro, $p->data_reuniao, $p->data_final); ?>
                    <div class="ui progress  barra-progresso <?=($p->status==2?'error':'');?>" data-percent="<?=$return['perc'];?>">
                        <div class="bar">
                            <div class="progress"></div>
                        </div>
                        <div class="label"></div>
                    </div>
                </td>
                <td><?=$return['prox'];?></td>
                <td><?=dtbr($p->data_cadastro);?></td>
            </tr>
            <? endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th>
                    <a href="<?=site_url('incluir')?>" class="btn btn-success"><div class="ui blue labeled icon button"><i class="user icon"></i>Cadastrar prospect</div></a>
                </th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th><?=sizeof($prospect)?></span> Prospect Cadastrados</th>
            </tr>
        </tfoot>
    </table>

<? $this->load->view('rodape'); ?>