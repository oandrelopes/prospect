<? $this->load->view('cabecalho')?>
<div id="clientsList">
    
    <h4>Clientes</h4>
    
    <div class="ui divider"></div>

    <? if($count != 0): ?>

        <form action="<?=site_url('clientes/filtrar')?>" method="post" class="filtro-lista">
            <div class="ui small action input">
                <input value="<?=@$busca?>" type="text" placeholder="Search..." class="input-busca" name="ed_filtro">
                <button type="submit" class="ui button small" value="Search">Search</button>
            </div>
        </form>


        <br clear="all" />

         <table class="ui table segment">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Endereço</th>
                    <th>CPF</th>
                </tr>
            </thead>
            <tbody>
                <? foreach($clients as $i => $c): ?>
                    <tr>
                        <td>
                            <a href="<?=site_url('clientes/atualizar/' . $c->id)?>"><?=$c->nome?></a><br />
                            <? if($c->email != ''): ?><strong>E-mail:</strong> <?=$c->email?><? endif;?>
                        </td>
                        <td>
                            <strong>Tel:</strong> <?=$c->tel1?>
                            <?=($c->tel2)?"<br /><strong>Tel2: </strong>".$c->tel2:""?>
                        </td>
                      
                        <td>
                            <?=$c->logradouro?> <?=$c->numero?> <?=$c->complemento?>, <?=$c->bairro?> - <?=$c->cidade?> - <?=$c->estado?><br />
                            <strong>CEP:</strong> <?=$c->cep?>
                        </td>
                        <td>
                            <strong></strong><br />
                            <? if($c->cpf != ''):?><strong></strong> <?=$c->cpf?><br /><? endif;?>
                        </td>
                    </tr>
                <? endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>
                        <a href="<?=site_url('clientes/incluir')?>" class="btn btn-success"><div class="ui blue labeled icon button"><i class="user icon"></i>Cadastrar Cliente</div></a>
                    </th>
                    <th></th>
                    <th></th>
                    
                    <th><span><?=sizeof($clients)?></span> Clientes</th>
                </tr>
            </tfoot>
           
        </table>

    <? else: ?>
        <p class="error">Nenhum cliente encontrado.</p>
        <a href="<?=site_url('clientes/incluir')?>" class="btn btn-success">
            <div class="ui blue labeled icon button"><i class="user icon"></i>Cadastrar Cliente</div>
        </a>
    <? endif; ?>

</div>

<?=@$this->pagination->create_links();?>

<? $this->load->view('rodape')?>