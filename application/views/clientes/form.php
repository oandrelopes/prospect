<?php 
    $this->load->view('cabecalho');
    
    $atualizar = false;
    if ( isset($cliente->nome) && $cliente->nome )
        $atualizar = true;

?>
      
    <h4><?=@$title?></h4>

    <div class="ui divider"></div>
    
    <form action="" method="post" role="form" class="ui loading form segment">

        <div class="field">
            <div class="ui radio checkbox">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="radio" type="radio" id="ed_tipo_f" checked name="ed_tipo" value="f" />
                <label for="">Pessoa Física</label>
            </div>
            <!-- <div class="ui radio checkbox">
                <input class="radio" type="radio" id="ed_tipo_j" name="ed_tipo" value="j" />
                <label for="">Pessoa Juridica</label>
            </div> -->
        </div>
         
        <div class="field">
           <label class="control-label" for="inputEmail">* Nome:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="100" value="<?=set_value('ed_nome', @$cliente->nome)?>" type="text" name="ed_nome" id="ed_nome" />
                <?=form_error('ed_nome')?>
            </div>
        </div>

        <div class="field" id="divCpf">
            <label class="control-label" for="inputCpf"> CPF:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="14" type="text" value="<?=set_value('ed_cpf', @$cliente->cpf)?>" name="ed_cpf" id="ed_cpf" /> 
                <?=form_error('ed_cpf')?>
            </div>
        </div>
        
        <!-- Modal aviso se cpf ja existe ou é invalido-->
        <div class="ui modal modal-cpf">
            <i class="close icon"></i>
            <div class="header">O CPF já existe</div>
            <div class="content">
                <p>O CPF digitado já está cadastrado no sistema, verefique o número novamente, ou procure pelo cliente…</p>
            </div>
            <div class="actions">
                <div class="ui black button">Fechar</div>
                <a href="<?= site_url('clientes') ?>" class="ui positive right labeled icon button">Ver Clientes</a>
            </div>
        </div>
        <!-- Modal aviso se cpf ja existe ou é invalido-->

        <div class="field">
            <label class="control-label" for="inputEmail">RG:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="14" type="text" value="<?=set_value('ed_rg', @$cliente->rg)?>" name="ed_rg" id="ed_rg" />
                <?=form_error('ed_rg')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"> Data de Nascimento:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="14" type="text" value="<?=set_value('ed_dataNascimento', @$cliente->dataNascimento)?>" name="ed_dataNascimento" id="ed_dataNascimento" />
                <?=form_error('ed_dataNascimento')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"> Nome da Mãe:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" type="text" value="<?=set_value('ed_nomeMae', @$cliente->nomeMae)?>" name="ed_nomeMae" id="ed_nomeMae" />
                <?=form_error('ed_nomeMae')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"> Nome do Pai:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control"  type="text" value="<?=set_value('ed_nomePai', @$cliente->nomePai)?>" name="ed_nomePai" id="ed_nomePai" />
                <?=form_error('ed_nomePai')?>
            </div>
        </div>    

        <div class="field">
            <label class="control-label" for="inputEmail">E-mail:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="100" value="<?=set_value('ed_email', @$cliente->email)?>" type="text" name="ed_email" id="ed_email" />
                <?=form_error('ed_email')?>
            </div>
        </div>
        
        <div class="field">
            <a class="ui labeled icon button mini" <?=(@$cliente->nome)?"style='display:none'":""?> id="add" title="Adicionar Telefone">Telefone <i class="expand icon"></i></a>
            <a class="ui right icon button mini" <?=(@$cliente->nome)?"style='display:none'":""?> id="remove" title="Remover Telefone"><i class="collapse icon"></i></a>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"></label>
            <div class="controls">
                    <input <?=(@$cliente->nome)?'disabled':''?> type="text" id="ed_telefone0" name="ed_telefone0" placeholder="Telefone 1" value="<?=set_value('ed_telefone0', @$telefones[0]->telefone)?>" class="form-control telefone"/>
                    <?=form_error('ed_telefone0')?>
            </div>
        </div>
        <!-- input hidden para poder verificar quantos inputs de telefones foram adicionados -->
        <input type="hidden" id="ed_total_tel" name="ed_total_tel" value="1">

        <div id="telefones" class="controls">
            <?if(@$telefones):?>
                <?foreach ($telefones as $key => $tel){
                    if($key>0)
                    echo  "<div class='field'><input disabled type='text' class='form-control telefone' id='ed_telefone".$key."' name='ed_telefone".$key."' placeholder='Telefone ".$key."'  value='".set_value('ed_telefone'.$key, @$tel->telefone)."' /></div>";
                }?>
            <?endif;?>
        </div>


        <h4>Endereço</h4>
        <div class="ui divider"></div>
        <div class="field">
            <label class="control-label" id="cep-label" for="inputEmail">* CEP:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> placeholder="Informe o CEP" class="form-control" size="10" maxlength="9" type="text" value="<?=set_value('ed_cep', @$cliente->cep)?>" name="ed_cep" id="ed_cep" />
                <?=form_error('ed_cep')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail">* Estado:</label>
            <div class="controls">
                 <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" style="text-transform: uppercase;" size="10" maxlength="9" type="text" value="<?=set_value('ed_uf', @$cliente->estado)?>" name="ed_uf" id="ed_uf" />                
                <?=form_error('ed_uf')?>
            </div>
        </div>        

        <div class="field">
            <label class="control-label" for="inputEmail">* Cidade:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="10" maxlength="50" type="text" value="<?=set_value('ed_cidade', @$cliente->cidade)?>" name="ed_cidade" id="ed_cidade" />
                <?=form_error('ed_cidade')?>
            </div>
        </div>
 
        <div class="field">
            <label class="control-label" for="inputEmail">* Bairro:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="20" type="text" value="<?=set_value('ed_bairro', @$cliente->bairro)?>" name="ed_bairro" id="ed_bairro" />
                <?=form_error('ed_bairro')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail">* Endereço:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="40" type="text" value="<?=set_value('ed_endereco', @$cliente->logradouro)?>" name="ed_endereco" id="ed_endereco" />
                <?=form_error('ed_endereco')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"> Número:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="5" type="text" value="<?=set_value('ed_numero', @$cliente->numero)?>" name="ed_numero" id="ed_numero" />
                <?=form_error('ed_numero')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail">Complemento:</label>
            <div class="controls">
                <input<?=(@$cliente->nome)?' disabled="disabled"':''?> class="form-control" size="20" type="text" value="<?=set_value('ed_complemento', @$cliente->complemento)?>" name="ed_complemento" id="ed_complemento" />
                <?=form_error('ed_complemento')?>
            </div>
        </div>

        <div class="form-actions">
            <div class="ui buttons">
                <a href="<?=site_url('clientes')?>" class="ui button cancelar">Voltar</a>
                <div class="or"></div>
                <div <?=($atualizar) ? '' : 'style="display:none;"'; ?> class="ui blue button atualizar_cliente">Atualizar</div>
                <button <?=($atualizar) ? 'style="display:none;"' : ''; ?> type="submit" class="ui positive button salvar_cliente">Salvar</button>
                
            </div>
        </div>

    </form>

<? $this->load->view('rodape'); ?>