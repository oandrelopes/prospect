<? $this->load->view('cabecalho')?>

    <h4>Metodos</h4>

    <hr />

    <table class="ui table segment">
        <thead>
            <tr>
                <th>Classe</th>
                <th>Método</th>
                <th>Apelido</th>
                <th>Método Privado <i class="info icon link popup_btn" data-content="Se método for privado (ON), o usuário requer permissão para acessa-lo. Se o método não estiver privado (OFF), qualquer usuário poderá acessa-lo, sem precisar de permição."></i></th>
            </tr>
        </thead>
        <tbody>        
            <?php foreach ($metodos as $key => $value) : ?>   
            <tr>
                <td><?php echo $value->classe; ?></td>
                <td><?php echo $value->metodo; ?></td>
                <td><span title="Editar" class="contenteditable edit_apelido" contenteditable="true" data-idmetodo="<?=$value->id;?>" ><?php echo $value->apelido; ?></span> <div title="Salvar Apelido" class="icon-salvar-apelido hidden"><i class="checkmark icon"></i></div></td>
                <td>
                    <div class="ui toggle checkbox checkbox_privacidade" <? echo ( $value->privado == 0 ) ? "title='Privado'" : "title='Permitido'" ?> data-idmetodo="<?=$value->id;?>" >
                        <input <? echo ( $value->privado == 0 ) ? "checked='checked'" : "" ?> type="checkbox" class="checkbox" id="check_privado" name="check_privado" />
                        <label></label>
                    </div>
                </td>
            </tr>
            <? endforeach;?>             
        </tbody>
        <tfoot>
            <tr>                
                <th></th>
                <th></th>
                <th></th>
                <th>Total:</th>                
            </tr>
        </tfoot>
    </table>

<? $this->load->view('rodape')?>