<? $this->load->view('cabecalho'); ?>
      
    <h4><?=@$title?></h4>

    <hr/>    
    
    <form action="" method="post" role="form" class="ui loading form segment">        

        <div class="field">
            <div class="ui radio checkbox">            
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="radio" type="radio" id="ed_tipo_f" checked name="ed_tipo" value="f" />
                <label for="">Pessoa Física</label>            
            </div>       
            <div class="ui radio checkbox">            
                <input class="radio" type="radio" id="ed_tipo_j" name="ed_tipo" value="j" />
                <label for="">Pessoa Juridica</label>            
            </div>            
        </div>
         
        <div class="field">
           <label class="control-label" for="inputEmail">* Nome:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="100" value="<?=set_value('ed_nome', @$client->nome)?>" type="text" name="ed_nome" id="ed_nome" />
                <?=form_error('ed_nome')?>
            </div>
        </div>

        <div class="field" id="divCpf">
            <label class="control-label" for="inputCpf"> CPF:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="14" type="text" value="<?=set_value('ed_cpf', @$client->cpf)?>" name="ed_cpf" id="ed_cpf" /> 
                <?=form_error('ed_cpf')?>
            </div>
        </div>
        
        <!-- Modal aviso se cpf ja existe ou é invalido-->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">O CPF já existe</h3>
            </div>
            <div class="modal-body">
                <p>O CPF digitado já está cadastrado no sistema, verefique o número novamente, ou procure pelo cliente…</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
                <a href="<?= site_url('clientes') ?>" class="btn btn-primary">Ver Clientes</a>
            </div>
        </div>
        <!-- Modal aviso se cpf ja existe ou é invalido-->


        <div class="field">
            <label class="control-label" for="inputEmail">RG:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="14" type="text" value="<?=set_value('ed_rg', @$client->rg)?>" name="ed_rg" id="ed_rg" />
                <?=form_error('ed_rg')?>
            </div>
        </div>
        <div class="field">
            <label class="control-label" for="inputEmail"> Data de Nascimento:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="14" type="text" value="<?=set_value('ed_dataNascimento', @$client->dataNascimento)?>" name="ed_dataNascimento" id="ed_dataNascimento" />
                <?=form_error('ed_dataNascimento')?>
            </div>
        </div>
        <div class="field">
            <label class="control-label" for="inputEmail"> Nome da Mãe:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" type="text" value="<?=set_value('ed_nomeMae', @$client->nomeMae)?>" name="ed_nomeMae" id="ed_nomeMae" />
                <?=form_error('ed_nomeMae')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"> Nome do Pai:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control"  type="text" value="<?=set_value('ed_nomePai', @$client->nomePai)?>" name="ed_nomePai" id="ed_nomePai" />
                <?=form_error('ed_nomePai')?>
            </div>
        </div>    

        <div class="field">
            <label class="control-label" for="inputEmail">E-mail:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="40" maxlength="100" value="<?=set_value('ed_email', @$client->email)?>" type="text" name="ed_email" id="ed_email" />
                <?=form_error('ed_email')?>
            </div>
        </div>

        <a class="btn btn-primary btn-xs" <?=(@$client->nome)?"style='display:none'":""?> id="add" title="Adicionar Telefone">Telefone <span class="glyphicon glyphicon-plus-sign"></span></a>
        <a class="btn btn-primary btn-xs" <?=(@$client->nome)?"style='display:none'":""?> id="remove" title="Remover Telefone"><span class="glyphicon glyphicon-minus-sign"></span></a>
           
        <div class="field">
            <label class="control-label" for="inputEmail">Telefones:</label>
            <div class="controls">                
                    <input <?=(@$client->nome)?'disabled':''?> type="text" id="ed_telefone0" name="ed_telefone0" placeholder="Telefone 1" value="<?=set_value('ed_telefone0', @$telefones[0]->telefone)?>" class="form-control"/>                
                    <?=form_error('ed_telefone0')?>
            </div>
        </div>
        <!-- input hidden para poder verificar quantos inputs de telefones foram adicionados -->
        <input type="hidden" id="ed_total_tel" name="ed_total_tel" value="1">

        <div id="telefones" class="controls">
            <?if(@$telefones):?>
                <?foreach ($telefones as $key => $tel){
                    if($key>0)
                    echo  "<div class='field'><input disabled type='text' class='form-control' id='ed_telefone".$key."' name='ed_telefone".$key."' placeholder='Telefone ".$key."'  value='".set_value('ed_telefone'.$key, @$tel->telefone)."' /></div>";
                }?>
            <?endif;?>
        </div>


        <h4>Endereço</h4>
        <hr/>
        <div class="field">
            <label class="control-label" id="cep-label" for="inputEmail">* CEP:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> placeholder="Informe o CEP" class="form-control" size="10" maxlength="9" type="text" value="<?=set_value('ed_cep', @$client->cep)?>" name="ed_cep" id="ed_cep" />
                <?=form_error('ed_cep')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail">* Estado:</label>
            <div class="controls">
                 <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" style="text-transform: uppercase;" size="10" maxlength="9" type="text" value="<?=set_value('ed_uf', @$client->estado)?>" name="ed_uf" id="ed_uf" />                
                <?=form_error('ed_uf')?>
            </div>
        </div>        

        <div class="field">
            <label class="control-label" for="inputEmail">* Cidade:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="10" maxlength="50" type="text" value="<?=set_value('ed_cidade', @$client->cidade)?>" name="ed_cidade" id="ed_cidade" />
                <?=form_error('ed_cidade')?>
            </div>
        </div>
 
        <div class="field">
            <label class="control-label" for="inputEmail">* Bairro:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="20" type="text" value="<?=set_value('ed_bairro', @$client->bairro)?>" name="ed_bairro" id="ed_bairro" />
                <?=form_error('ed_bairro')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail">* Endereço:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="40" type="text" value="<?=set_value('ed_endereco', @$client->logradouro)?>" name="ed_endereco" id="ed_endereco" />
                <?=form_error('ed_endereco')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail"> Número:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="5" type="text" value="<?=set_value('ed_numero', @$client->numero)?>" name="ed_numero" id="ed_numero" />
                <?=form_error('ed_numero')?>
            </div>
        </div>

        <div class="field">
            <label class="control-label" for="inputEmail">Complemento:</label>
            <div class="controls">
                <input<?=(@$client->nome)?' disabled="disabled"':''?> class="form-control" size="20" type="text" value="<?=set_value('ed_complemento', @$client->complemento)?>" name="ed_complemento" id="ed_complemento" />
                <?=form_error('ed_complemento')?>
            </div>
        </div>

        <div class="form-actions">           
            <? if(@$client->nome):?>
                <input type="button" id="atualizar_cliente" class="btn btn-primary" value="Alterar Dados" /> 
                <div class="pull-right">
                    <input type="button" id="cadastrar_paciente" class="btn btn-success" value="Cadastrar Paciente" onclick="window.open('<?=site_url('paciente/new_paciente/').'/'.$this->uri->segment(3)?>')"/>
                </div>               
            <? endif;?>
                <input<?=(@$client->nome)?' style="display:none;"':'';?> type="submit" id="salvar_cliente" class="btn btn-primary" value="Salvar" /> 
                <a class="btn btn-default" href="<?=site_url('clientes')?>" value="Cancelar">Cancelar</a>
                
           
        </div>
        
    </form>

<? $this->load->view('rodape'); ?>