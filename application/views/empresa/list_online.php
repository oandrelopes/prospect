<? $this->load->view('cabecalho'); ?>
    <h1><?=$title?></h1>
    <hr/>
    
    <table class="table table-striped">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>IP</th>
                  <th>Última Interação</th>
                  <th>Tipo de Usuário</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
	              <? 
            foreach($users as $i => $u):
           		$obj      = unserialize($u->user_data);
           		@$usuario = unserialize($obj['usuario']);
           		$data     = getdate($u->last_activity);
           		if(!@$usuario) continue;	
            ?>
            <tr>
                <td><?=@$usuario->nome?></td>
                <td><?=@$usuario->email?></td>
                <td><?=$u->ip_address?></td>
                <!--<td><?//=$u->user_agent?></td>-->
                <td><?=$data['mday'] . '/' . $data['wday'] . '/' . $data['year'] . ' ' . $data['hours'] . ':' . $data['minutes'] . ':' . $data['seconds']?></td>
                <td><?=(@$usuario->cod_tipo == 1)?'Administrador':'Usu&aacute;rio Comum'?></td>
                <td><a onclick="return confirm('Deseja realmente deslogar o(a) usuário(a) <?=@$usuario->nome?>?')" href="<?=site_url('users/logoff/' . $u->session_id)?>" class="btn">Deslogar</a></td>
            </tr>
            <? endforeach;?>
              </tbody>
            </table>
    
<div class="well">
	<span class="badge badge-success"><?=sizeof($users)?></span> usuários online
</div>

<? $this->load->view('rodape'); ?>