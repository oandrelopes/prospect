<? $this->load->view('cabecalho'); ?>
    <h1>Alterar senha</h1>
    <hr/>
    <form action="" method="post" class="form-horizontal">
    
    	<div class="control-group">
	    	<label class="control-label" for="inputEmail">Senha Antiga</label>
	    	<div class="controls">
		    	<input size="40" maxlength="100" autocomplete="off" type="password" name="ed_asenha" id="ed_asenha" class="input-block-level"/>
		    </div>
    	</div>
    	
    	<div class="control-group">
	    	<label class="control-label" for="inputEmail">Nova Senha</label>
	    	<div class="controls">
		    	<input size="40" maxlength="100" autocomplete="off" type="password" name="ed_nsenha" id="ed_nsenha" class="input-block-level"/>
		    </div>
    	</div>
    	
    	<div class="control-group">
	    	<label class="control-label" for="inputEmail">Repita a Nova Senha</label>
	    	<div class="controls">
		    	<input size="40" maxlength="100" autocomplete="off" type="password" name="ed_rsenha" id="ed_rsenha" class="input-block-level"/>
	            <p class="error"><?=(@$msg != null)?$msg:''?></p>
		    </div>
    	</div>
    	
    	<div class="form-actions">
	    	<button type="submit" class="btn btn-primary">Salvar</button>
	    </div>

    </form>

<? $this->load->view('rodape'); ?>