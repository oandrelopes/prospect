<? 
    $this->load->view('cabecalho');
?>
    <h4>Empresas</h4>
    
    <div class="ui divider"></div>

    <table class="ui table segment">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Slug</th>
                <th>Cnpj</th>
            </tr>
        </thead>
        <tbody>
            <? foreach($empresas as $i => $e) : ?>
            <tr>
                <td><a href="<?=site_url('empresa/atualizar/' . $e->id)?>"><?=$e->nome?></a></td>
                <td><?=$e->slug?></td>
                <td><?=$e->cnpj?></td>
            </tr>
            <? endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th>
                    <a href="<?=site_url('empresa/incluir')?>" class="btn btn-success"><div class="ui blue labeled icon button"><i class="user icon"></i>Cadastrar Empresa</div></a>
                </th>
                <th></th>
                <th><?=sizeof($empresas)?></span> Empresas Cadastrados</th>
            </tr>
        </tfoot>
    </table>

<? $this->load->view('rodape'); ?>