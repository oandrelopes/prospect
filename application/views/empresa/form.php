<? $this->load->view('cabecalho'); ?>

    <h4><?php echo $title;?></h4>
    
    <div class="ui divider"></div>
    
    <form action="" method="post" class="ui loading form segment">
    
    	<div class="field box-slug" style="<?=( isset($empresa->nome) && $empresa->nome ) ? 'display:block' : ''; ?>">
	    	<div class="controls">
		    	
		    	<div class="ui action input">
		    		<?php if ( isset($empresa->slug) && $empresa->slug ) : $url =  site_url('inicio/'); ?>
		    			<a href="<?php echo $url.'/'.$empresa->slug; ?>" target="_blank"><?php echo $url.'/'.$empresa->slug; ?></a>
		    		<?php endif; ?>
				</div>
	    		<div class="ui action input">
		    		<input placeholder="Url Amigavel" size="40" maxlength="100" value="<?=set_value('ed_slug', @$empresa->slug)?>" type="text" name="ed_slug" id="ed_slug" class="input-block-level" readonly="readonly"/>
			    	<div class="ui icon button editar-slug"><i class="edit icon"></i></div>
				</div>

		    	<?=form_error('ed_slug')?>
		    </div>
		</div>

    	<div class="field">
	    	<label>Nome</label>
	    	<div class="controls">
		    	<input value="<?=set_value('ed_nome', @$empresa->nome)?>" type="text" name="ed_nome" id="ed_nome" class="input-block-level"/>
		    	<?=form_error('ed_nome')?>
		    </div>
		</div>
		
		<div class="field">
	    	<label>Cnpj</label>
	    	<div class="controls">
		    	 <input size="40" type="text" value="<?=set_value('ed_cnpj', @$empresa->cnpj)?>" name="ed_cnpj" id="ed_cnpj" class="input-block-level"/>
		    	 <?=form_error('ed_cnpj')?>
		    </div>
		</div>

		<div class="field">
	    	<label>Sobre</label>
	    	<div class="controls">
		    	 <textarea size="40" type="text" value="<?=set_value('ed_sobre', @$empresa->sobre)?>" name="ed_sobre" id="ed_sobre" class="input-block-level"><?=set_value('ed_sobre', @$empresa->sobre)?></textarea>
		    	 <?=form_error('ed_sobre')?>
		    </div>
		</div>

		<div class="field">
	    	<label>Endereço</label>
	    	<div class="controls">
		    	 <input size="40" type="text" value="<?=set_value('ed_endereco', @$empresa->endereco)?>" name="ed_endereco" id="ed_endereco" class="input-block-level"/>
		    	 <?=form_error('ed_endereco')?>
		    </div>
		</div>

		<div class="field">
	    	<label>E-mail</label>
	    	<div class="controls">
		    	 <input size="40" type="text" value="<?=set_value('ed_email', @$empresa->email)?>" name="ed_email" id="ed_email" class="input-block-level"/>
		    	 <?=form_error('ed_email')?>
		    </div>
		</div>

		<div class="inline field">
	    	<div class="ui slider checkbox">
		    	<input <? if ( isset($empresa) && $empresa->status==1 ) { echo "checked='checked'"; } elseif ( isset($empresa) && $empresa->status==0 ){echo "";} else {echo "checked='checked'";}?> type="checkbox" class="checkbox" id="ed_status" name="ed_status" />
	    		<label>Empresa Status</label>
		    </div>
		</div>

		<div class="form-actions">
			<div class="ui buttons">
			  	<a href="<?=site_url('empresa')?>" class="ui button cancelar">Cancel</a>
			  	<div class="or"></div>
			  	<button type="submit" class="ui positive button salvar">Salvar</button>
			</div>			
		</div>

    </form>
		

<? $this->load->view('rodape'); ?>