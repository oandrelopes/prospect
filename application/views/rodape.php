    </div><!-- .main container -->
    <footer>
        <script src="<?=site_url('/public/js/jquery-2.1.0.min.js');?>"></script>
        <script src="<?=site_url('/public/semantic/javascript/semantic.js');?>"></script>
        <script src="<?=site_url('/public/js/default.js');?>"></script>
        <? if ( isset($js) && $js ) echo js('/public/js', $js) ?>

        <div class="mensagem-ajax">
            <div class="ui compact small green message">Permissão Salva</div>
            <div class="ui hidden compact small red message">Erro ao salvar</div>
        </div>

    </footer>

</body>
</html>