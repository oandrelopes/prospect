<?php
function css($endereco = '', $lista_css = ''){
	
    $lista_css = explode('|', $lista_css);

    $r = '';
    if($lista_css[0] != ''){
        for($i = 0 ; $i < sizeof($lista_css) ; $i++){
            $r .= '<link href="' . site_url($endereco . '/' . $lista_css[$i]) . '" type="text/css" rel="stylesheet" />' . "\n";
        }
    }
    return $r;
}

function js($endereco = '', $lista_js = ''){

    $lista_js = explode('|', $lista_js);

    $r = '';
    if($lista_js[0] != ''){
        for($i = 0 ; $i < sizeof($lista_js) ; $i++){
            $r .= '<script type="text/javascript" src="' . site_url($endereco . '/' . $lista_js[$i]) . '"></script>' . "\n";
        }
    }

    return $r;
}

function cp($cad, $reu, $fim = '') {

    $da = date('Y-m-d');/*DATA ATUAL*/

    $date = date_create($cad);
    $cad = date_format($date,"Y-m-d");


    if ( validateDate($fim) && $da >= $fim ) {
        $return['perc'] = 100;
        $return['prox'] = 'Finalizado';
    } else if ( validateDate($reu) && $da >= $reu ) {
        $return['perc'] = 66;
        $return['prox'] = 'Prazo final: '.dtbr($fim);
    } else if ( validateDate($cad) && validateDate($reu) && $da >= $cad ) {
        $return['perc'] = 33;
        $return['prox'] = (validateDate($reu)?'Data da reunião: '.dtbr($reu):'Marcar reunião');
    } else {
        $return['perc'] = 0.0;
        $return['prox'] = 'Marcar reunião';
    }

    return $return;

}

function validateDate($date) {

    $date = explode("-",$date); // fatia a string $dat em pedados, usando / como referência
    $y = $date[0];
    $m = $date[1];
    $d = $date[2];
 
    // verifica se a data é válida!
    // 1 = true (válida)
    // 0 = false (inválida)
    $res = checkdate($m,$d,$y);
    if ($res == 1){
       return true;
    } else {
       return false;
    }

}

function dtbr($data) { /*DATA BRASIL*/
    return date_format(date_create($data),"d/m/Y");
}
