<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class Permissoes
    {
     
        private $ci;
        public function __construct() {
            /*
            * Criando uma instância do CodeIgniter para poder acessar
            * banco de dados, sessionns, models, etc...
            */
            $this->ci =& get_instance();
            $this->logado = false;
        }
        
        function check_logged($classe,$metodo) {
           
            /**
            * Buscando a classe e metodo da tabela metodos
            */
            $array = array('classe' => $classe, 'metodo' => $metodo);
            $this->ci->db->where($array);
            $query = $this->ci->db->get('metodos');
            $result = $query->result();

            // Se este metodo ainda não existir na tabela sera cadastrado
            if ( count($result)==0 ) {
                $data = array(
                    'classe' => $classe ,
                    'metodo' => $metodo ,
                    'apelido' => $classe .  '/' . $metodo,
                    'privado' => 0
                );
                $this->ci->db->insert('metodos', $data);
                redirect(base_url(). $classe . '/' . $metodo, 'refresh');
            
            } else {
            //Se ja existir tras as informacoes de publico ou privado
            
                if( $result[0]->privado == 1 ){
                    // Escapa da validacao e mostra o metodo.
                    return true;
                } else {
                    // Se for privado, verifica o login
                    $nome = $this->ci->session->userdata('name'); 
                    $logged_in = $this->ci->session->userdata('isLoggedIn');
                    $data = $this->ci->session->userdata('user_data'); 
                    $email = $this->ci->session->userdata('email');
                    $id_usuario =  $this->ci->session->userdata('id');
                    $id_metodos = $result[0]->id;
                    // Se o usuario estiver logado vai verificar se tem permissao na tabela.
                    if ( $nome && $logged_in && $id_usuario ) {
                        $array = array('id_metodo' => $id_metodos, 'id_usuario' => $id_usuario);
                        $this->ci->db->where($array);
                        $query2 = $this->ci->db->get('permissoes');
                        $result2 = $query2->result();
                        // Se não vier nenhum resultado da consulta, manda para página de
                        // usuario sem permissão.

                        if ( $id_usuario != 1 && count($result2) == 0 ) {
                            return false;
                        } else {
                            return true;
                        }
                     }
                     // Se não estiver logado, sera redirecionado para o login.
                     else {
                            redirect(base_url().'login', 'refresh');
                        }
                    }
                }
            }


            function get( $p ) {
                $this->verificar();
                if ( $this->logado ) {
                    if ( ($this->user ) )
                        return $this->user;
                }
            }

            function verificar(){

                if (!$this->ci->session->userdata('id'))
                    return;

                $u = $this->ci->session->userdata('id');

                if( $u >= 1 ) {
                    $this->user   = $u;
                    $this->logado = true;
                } else {
                    $this->ci->session->unset_userdata('id');
                    $this->logado = false;
                }
            }


            function listar_permissoes( $id_usuario = NULL ) {

                $sql = sprintf("SELECT m.id, m.classe, m.metodo, m.apelido,
                            CASE p.id_usuario 
                                WHEN p.id_usuario THEN '1' 
                                ELSE '0' 
                            END AS permissao 

                        FROM metodos m 
                        LEFT JOIN permissoes p ON m.id = p.id_metodo AND p.id_usuario = '%s'
                        WHERE privado = 0
                        ORDER BY m.id ", $id_usuario);

                $query = $this->ci->db->query($sql);
                $result = $query->result();
                return $result;
            }

            function salvar_permissoes($id_user, $id_metodo, $ativo) {
                
                if ( $ativo && ( $ativo == 'true' || $ativo == true ) ) {
                    $data = array(
                        'id_metodo'  => $id_metodo,
                        'id_usuario' => $id_user
                    );
                    $result = $this->ci->db->insert('permissoes', $data);
                    return $result;
                } else {
                    $data = array(
                        'id_metodo'  => $id_metodo,
                        'id_usuario' => $id_user
                    );
                    $result = $this->ci->db->delete('permissoes', $data);
                    return $result;
                }
                
            }

            /**
            * Método auxiliar para autenticar entradas em menu.
            * Não faz parte do plugin como um todo.
            */
            function check_menu($classe,$metodo) {
                $this->ci =& get_instance();
                $sql = "SELECT SQL_CACHE
                 count(permissoes.id) as found
                 FROM
                 permissoes
                 INNER JOIN metodos
                 ON metodos.id = permissoes.id_metodo
                 WHERE id_usuario = '" . $this->ci->session->userdata('id_usuario') . "'
                 AND classe = '" . $classe . "'
                 AND metodo = '" . $metodo . "'";
                $query = $this->ci->db->query($sql);
                $result = $query->result();
                return $result[0]->found;

                /*SELECT SQL_CACHE classe, metodo from permissoes p
                INNER JOIN metodos m ON m.id =  p.id_metodo OR m.privado = 1
                where id_usuario = 2*/
            }

            function active_menu( $menu ) {
                if ( $this->uri->segment(1)==$menu ) {
                    return "active";
                }
            }

            function logout() {
                $this->ci->session->unset_userdata('session_id');
                $this->logado = false;
                $this->user = new stdClass();
            }

            function get_empresa($slug) {
                $query = $this->ci->db->query("SELECT id FROM empresa WHERE slug = ? ", $slug);
                return $query->row();
            }



    }