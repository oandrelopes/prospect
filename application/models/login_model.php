<?php 
    class Login_model extends CI_Model {
    
        var $details;        

        function validate_user( $email, $password ) {
            // Build a query to retrieve the user's details
            // based on the received username and password
            $this->db->from('usuario');
            $this->db->where('email',$email );
            
            $login = $this->db->get()->result();             
            
            // The results of the query are stored in $login.
            // If a value exists, then the user account exists and is validated
            if ( isset($login[0]->senha) && is_array($login) && ( $this->encrypt->decode( $login[0]->senha ) == $password ) && count($login) == 1 ) {
                // Set the users details into the $details property of this class
                $this->details = $login[0];
                // Call set_session to set the user's session vars via CodeIgniter
                $this->set_session();
                return true;
            }

            return false;
        }

        function set_session() {
            // session->set_userdata is a CodeIgniter function that
            // stores data in a cookie in the user's browser.  Some of the values are built in
            // to CodeIgniter, others are added (like the IP address).  See CodeIgniter's documentation for details.
            $this->session->set_userdata( array(
                    'id'         => $this->details->id,
                    'name'       => $this->details->nome,
                    'email'      => $this->details->email,
                    'id_empresa' => $this->details->id_empresa,
                    #'avatar'=>$this->details->avatar,
                    #'tagline'=>$this->details->tagline,
                    #'isAdmin'=>$this->details->isAdmin,
                    #'teamId'=>$this->details->teamId,
                    'isLoggedIn'=>true
                )
            );
        }

    }