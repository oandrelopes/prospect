<?php

class Agenda_Model extends CI_Model 
{

    public function listar_principal( $slug_empresa ) {
        $query = $this->db->query("SELECT
                                        id_cliente AS title,
                                        data_inicial AS start,
                                        data_final AS end,
                                    CASE agenda.`status`
                                    WHEN '1' THEN
                                        '#00D3B1'
                                    ELSE
                                        '#DDD'
                                    END AS color
                                    FROM
                                        agenda
                                    INNER JOIN empresa ON empresa.id = id_empresa
                                    AND empresa.slug = ?
                                    ORDER BY
                                        id_categoria,
                                        data_inicial", $slug_empresa);
                                            return $query->result();
    }

    public function listar() {
        $query = $this->db->query("SELECT
                                        *
                                    FROM
                                        agenda
                                        WHERE
                                        id_empresa = ?
                                    ORDER BY
                                        id_categoria,
                                        data_inicial", $this->idEmpresa);
        return $query->result(); 
    }

    public function incluir(){
        return $this->db->query("INSERT INTO 
                                    agenda (id_empresa, id_cliente, id_categoria, data_inicial, data_final, data_cadastro, status) 
                                    VALUES (?, ?, ?, ?, ?, ?, ?)", 
                                    array( $this->id_empresa, $this->id_cliente, $this->id_categoria, $this->data_inicial, $this->data_final, $this->data_cadastro, $this->status ) );
    }

    public function atualizar_status() {
        return $this->db->query("UPDATE agenda 
                                    SET status = ? 
                                    WHERE id = ?", array( $this->status, $this->id_agenda ) );
    }

    public function atualizar_empresa(){
        return $this->db->query("UPDATE empresa SET nome = ?, slug = ?, cnpj = ?, status = ? WHERE id = ?", array($this->nome, $this->slug, $this->cnpj, $this->status, $this->id_empresa) );
    }

    public function busca_empresa($id_empresa) {
        $query = $this->db->query('SELECT * FROM empresa WHERE id = ? ', $id_empresa);
        return $query->row();
    }

    public function create_unique_slug($string, $table) {
        $slug = url_title($string);
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params['slug'] = $slug;
        if ($this->input->post('id')) {
            $params['id !='] = $this->input->post('id');
        }
        
        while ($this->db->where($params)->get($table)->num_rows()) {
            if (!preg_match ('/-{1}[0-9]+$/', $slug )) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            }
            $params ['slug'] = $slug;
            }
        return $slug;
    } 

}