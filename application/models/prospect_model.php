<?php

    class Prospect_Model extends CI_Model {

        function listar($ativo = 1){

            $query = $this->db->query("SELECT
                                            p.*, ph.data_reuniao, ph.data_final
                                        FROM
                                            prospect p
                                        INNER JOIN prospect_historico ph ON p.id = ph.id_prospect
                                        WHERE
                                            id_empresa = ?
                                        AND ph.id = (SELECT MAX(id) FROM prospect_historico WHERE id_prospect = p.id)
                                        ORDER BY
                                            p.id DESC", $this->idEmpresa);

            return $query->result();
        }

        function incluir() {
            $data_atual = date('Y-m-d H:i:s');
            $this->db->query("INSERT INTO prospect (nome, setor, contato, data_cadastro, id_empresa ) VALUES (?, ?, ?, ?, ?)", array($this->nome, $this->setor, $this->contato, $data_atual, $this->idEmpresa ));
            return $this->db->query("INSERT INTO prospect_historico (data_contato, data_reuniao, data_final, id_prospect ) VALUES (?, ?, ?, ?)", array( $data_atual, $this->data_reuniao, $this->data_final, $this->db->insert_id() ));
        }

        function buscar($id){
            $query = $this->db->query("SELECT
                                            *
                                        FROM
                                            prospect p
                                        INNER JOIN prospect_historico ph ON p.id = ph.id_prospect
                                        WHERE
                                            id_empresa = ?
                                        AND p.id = ?
                                        AND ph.id = (SELECT MAX(id) FROM prospect_historico WHERE id_prospect = p.id)", 
                                        array($this->idEmpresa, $id) );

            return $query->row();
        }



        function atualizar(){
            $this->db->query("UPDATE prospect SET nome = ?, setor = ?, contato = ?, status = ? WHERE id = ?", array($this->nome, $this->setor, $this->contato, $this->status , $this->id));
            return $this->db->query("INSERT INTO prospect_historico (data_contato, data_reuniao, data_final, id_prospect ) VALUES (?, ?, ?, ?)", array( $data_atual, $this->data_reuniao, $this->data_final, $this->id ));
        }

        function onlineList(){
            $query = $this->db->query('SELECT * FROM session');
            return $query->result();
        }

        function pegar_user_online(){
            $query = $this->db->query('SELECT * FROM session where session_id  = ?', $this->id_sessao );
            return $query->result();
        }

        function logOff(){
            return $this->db->query('DELETE FROM session WHERE session_id = ?', array($this->user_id));
        }
    }