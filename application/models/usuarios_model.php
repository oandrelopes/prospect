<?php

    class Usuarios_Model extends CI_Model {

        function dropUser(){
            return $this->db->query("UPDATE usuario SET ativo = '0' WHERE id = ?", array($this->cod_usuario));
        }

        function getUser($cod_user){
            $query = $this->db->query('SELECT * FROM usuario WHERE id = ?', array($cod_user));
            return $query->row();
        }

        function listar($ativo = 1){

            $query = $this->db->query("SELECT id, nome, email, id_permissao,
                                           CASE ativo
                                           WHEN '1' THEN 'Sim'
                                           ELSE 'Não'
                                           END AS ativo
                                       FROM usuario 
                                            WHERE id_empresa = ?
                                            AND ( ativo = 1 
                                            ||  ativo = 0 )
                                       ORDER BY nome ASC", $this->idEmpresa);
            return $query->result(); 
        }

        function incluir() {
            return $this->db->query("INSERT INTO usuario (nome, email, senha, ativo, id_permissao, id_empresa ) VALUES (?, ?, ?, ?, ?, ?)", array($this->nome, $this->email, $this->encrypt->encode($this->senha), $this->ativo, $this->tipo_usuario, $this->idEmpresa ));
        }

        function atualizar(){
            return $this->db->query("UPDATE usuario SET nome = ?, email = ?, ativo = ?, id_permissao = ? WHERE id = ?", array($this->nome, $this->email, $this->ativo, $this->tipo_usuario , $this->cod_usuario));
        }

        function updatePassword(){             
            $query = $this->db->query("SELECT * FROM usuario WHERE id = ? AND ativo = '1' LIMIT 1", array($this->permissoes->get('id')));
            if ($query->num_rows() <= 0)
                return false;

            $row = $query->row();            
            if ( strcmp( $this->asenha, $this->encrypt->decode($row->senha) ) === 0 ) {
                $this->user = $row;
                $this->db->query('UPDATE usuario SET senha = ? WHERE id = ?', array( $this->encrypt->encode($this->nsenha), $this->permissoes->get('id') ));
                return true;
            } else
                return false;   
        }

        function onlineList(){
        	$query = $this->db->query('SELECT * FROM session');
        	return $query->result();
        }

        function pegar_user_online(){
            $query = $this->db->query('SELECT * FROM session where session_id  = ?', $this->id_sessao );
            return $query->result();
        }

        function logOff(){
        	return $this->db->query('DELETE FROM session WHERE session_id = ?', array($this->user_id));
        }
    }