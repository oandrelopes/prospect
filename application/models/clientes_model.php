<?php

class Clientes_Model extends CI_Model{
    
     function cityList($estado){
        if($estado == '0')
            $estado = 'PR';

        $query = $this->db->query("SELECT cod_cidade, nome FROM cidade WHERE uf = UPPER(?)", array($estado));
        return $query->result();
    }

    function listar($posicao, $quantidade) {
        
        $sql = "SELECT *,
                (SELECT telefone FROM telefone WHERE idCliente = cl.id LIMIT 1) tel1, 
                (SELECT telefone FROM telefone WHERE idCliente = cl.id LIMIT 1 OFFSET 1) tel2
                   FROM cliente cl WHERE cl.id_empresa = ". $this->idEmpresa;

        if ( isset($this->filter) && $this->filter ) 
            $sql .= " AND cl.nome like '%".$this->filter."%' ";
            
            $sql .= " ORDER BY id DESC
                     LIMIT $quantidade 
                     OFFSET $posicao " ;

        $query = $this->db->query($sql);
        
        // Conta quantos clientes est�o cadastrados
        $query_count = $this->db->query('SELECT COUNT(*) AS total FROM cliente');
        $count = $query_count->row();
        $this->clientCount = $count->total;
        
        return $query->result();
    }
    
    function dropClient(){
        return $this->db->query('DELETE FROM cliente WHERE cod_cliente = ?', array($this->cod_cliente));
    }

    function clientFilter(){
        $query = $this->db->query('SELECT * FROM cliente WHERE nome like ?','%'.$this->filter.'%' );
        
        return $query->result();
    }

    function getClient($idCliente){
        $query = $this->db->query('SELECT c.* FROM cliente c WHERE c.id = ? ', $idCliente);
        $queryTel = $this->db->query('SELECT * FROM telefone WHERE idCliente = ? ORDER BY id ',$idCliente);
        $this->telefones = $queryTel->result();
        
        return $query->row();
    }

    function incluir() {
        // Verifica o tipo de pessoa
        if($this->tipo == 'f')
            // insere os campos apropriados caso seja jur�dica
            $resultado = $this->db->query('INSERT INTO cliente (nome, cpf, rg, dataNascimento, nomeMae, nomePai, email, cep, estado, cidade, bairro, logradouro, numero, complemento, dataCadastro, id_empresa ) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
            array(
                $this->nome,
                $this->cpf,
                $this->rg,
                $this->dataNascimento,
                $this->nomeMae,
                $this->nomePai,
                $this->email,
                $this->cep,
                $this->estado,
                $this->cidade,
                $this->bairro,
                $this->logradouro,
                $this->numero,
                $this->complemento,
                $this->data_cadastro,
                $this->idEmpresa
            ));
        elseif ( $this->tipo == 'j') {
        
            // insere os campos apropriados caso seja f�sica
            $resultado = $this->db->query('INSERT INTO cliente ( nome, email, telefone, celular, cep, cod_cidade, bairro, endereco, numero, complemento, cnpj, razao_social, tipo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', array(
              
                $this->nome,
                $this->email,
                $this->telefone,
                $this->celular,
                $this->cep,
                $this->cod_cidade,
                $this->bairro,
                $this->endereco,
                $this->numero,
                $this->complemento,
                $this->cnpj,
                $this->razao_social,
                't'
            ));
        }

        return $this->db->insert_id();

    }

    function newTelefone($idCliente,$telefones){
      
        foreach ($telefones as $key => $tel) {
            $this->db->query('INSERT INTO telefone(idCliente, telefone) VALUES (?,? )',array($idCliente,$tel));
        }
       

    }

    function updateTelefone($idCliente,$telefones){
        
        $this->db->where('idCliente', $idCliente);
        $this->db->delete('telefone');  

        foreach ($telefones as $key => $tel) {
            $this->db->query('INSERT INTO telefone(idCliente, telefone) VALUES (?,? )',array($idCliente,$tel));
        }
       

    }

    function stateList(){
        $query = $this->db->query('SELECT * FROM estado');
        return $query->result();
    }

    function updateClient(){
        if($this->tipo == 'f')
            return $this->db->query("UPDATE cliente SET nome = ?, cpf = ?, rg = ?, dataNascimento = ?, nomeMae = ?, nomePai = ?, email = ?, cep = ?, estado = ?, cidade = ?, bairro = ?, logradouro = ?, numero = ?, complemento = ? WHERE id = ?", 
            array(
                $this->nome,
                $this->cpf,
                $this->rg,
                $this->dataNascimento,
                $this->nomeMae,
                $this->nomePai,
                $this->email,
                $this->cep,
                $this->estado,
                $this->cidade,
                $this->bairro,
                $this->logradouro,
                $this->numero,
                $this->complemento,
                $this->idCliente
            ));
        else
            return $this->db->query("UPDATE cliente SET nome = ?, email = ?, cep = ?, cod_cidade = ?, bairro = ?, endereco = ?, numero = ?, complemento = ?, tipo = 't', cnpj = ?, razao_social = ? WHERE cod_cliente = ?", array(
                $this->nome,
                $this->email,
                $this->cep,
                $this->cod_cidade,
                $this->bairro,
                $this->endereco,
                $this->numero,
                $this->complemento,
                $this->cnpj,
                $this->razao_social,
                $this->cod_client
            ));
    }

    function vereficaCpf(){
        if($this->uri->segment(3))
            $cpf = $this->uri->segment(3);
        elseif($this->cpf)
            $cpf = $this->cpf;
        else
            $cpf = 1;
        $query = $this->db->query('SELECT * FROM cliente WHERE  cpf = ?', $cpf);

        if($query->row())
            return true;
        else
            return false;
    }
}