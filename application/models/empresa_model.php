<?php

class Empresa_Model extends CI_Model 
{


    public function listar_empresas($ativo = 1){
        $query = $this->db->query("SELECT *
                                   FROM empresa where id = ?
                                   ORDER BY nome ASC", $this->idEmpresa);
        return $query->result(); 
    }

    public function incluir_empresa() {
        $this->db->query("INSERT INTO empresa (nome, slug, cnpj, sobre, endereco, email, status) VALUES (?, ?, ?, ?, ?, ?, ?)", array($this->nome, $this->slug, $this->cnpj, $this->sobre, $this->endereco, $this->email, $this->status) );
        return $this->db->insert_id();
    }

    public function atualizar_empresa(){
        return $this->db->query("UPDATE empresa SET nome = ?, slug = ?, cnpj = ?, sobre = ?, endereco = ?, email = ?, status = ? WHERE id = ?", array($this->nome, $this->slug, $this->cnpj, $this->sobre, $this->endereco, $this->email, $this->status, $this->id_empresa) );
    }

    public function busca_empresa($busca) {
        $query = $this->db->query('SELECT * FROM empresa WHERE id = ? || slug = ? ', array( $busca, $busca) );
        return $query->row();
    }

    public function create_unique_slug( $slug ) {
        $slug = url_title( convert_accented_characters($slug) );
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params['slug'] = $slug;
        
        $this->db->where($params)->get('empresa')->num_rows();

        while ( $this->db->where($params)->get('empresa')->num_rows() ) {
            if ( !preg_match ('/-{1}[0-9]+$/', $slug ) ) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            }
            $params ['slug'] = $slug;
        }
        return $slug;
    } 

}